/*************************************************************************
Copyright (c) 2015, Haitham ASHOOR

>>> SOURCE LICENSE >>>
This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation (www.fsf.org); either version 2 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

A copy of the GNU General Public License is available at
http://www.fsf.org/licensing/licenses

>>> END OF LICENSE >>>
*************************************************************************/

#include "DensityBuilder.h"
#include "CNCalculator.h"
#include "ConditionsHandler.h"
#include "GCprofiler.h"
#include "Normalizer.h"

#include "utils.h"
#include "segmentation.h"
#include "FragLengthCalculator.h"


#include "dataanalysis.h"
#include <algorithm>
#include <iterator>
#include <set>
#include <cmath>
#include <cfloat>
#include <limits>
using namespace std;


#include <gsl/gsl_cdf.h>

ConditionsHandler::ConditionsHandler(int bin,int large,int med1,int min1,int max1,int med2,int min2,int max2,
		int merge,float p_threshold,
		float _fold_change,
		string& p, string& index, vector<BedEntry>& _black_list, bool _negbinom, bool _pairedEnd, bool ifSpikeIns): black_list(_black_list) {
	bin_size = bin;
	large_bin_size = large;
	median_f_length[0] = med1;
	min_f_length[0] = min1;
	max_f_length[0] = max1;
	median_f_length[1] = med2;
	min_f_length[1] = min2;
	max_f_length[1] = max2;
	min_dist = merge/bin_size;
	threshold = p_threshold;
	conditions_count = 0;
	path = p;
	gc_index = index;
	pairedEnd = _pairedEnd;
	if (pairedEnd){//will set fragment lengths to zero
			min_f_length[0] = min_f_length[1] = 0;
			median_f_length[0] = median_f_length[1] = 0;
			max_f_length[0] = max_f_length[1] = 0;

	}
	useSpikesCounts=ifSpikeIns;
    if (useSpikesCounts) {
        cout<<"Using spike-in values to normalize .wig files"<<endl;
    }

	max_obs = 0;
	min_obs = numeric_limits<int>::max();
	total_counts = 0;
	rows = 0;
	fold_change = _fold_change;

	negbinom = _negbinom;

	chr_list = vector<string>(3000,""); //TODO - should not hard code it..



	for (int i=0;i<3;i++)
		transitions.push_back(vector<float>(3,0));

}

ConditionsHandler::~ConditionsHandler() {
	// TODO Auto-generated destructor stub
}




void ConditionsHandler::clean_reads(reads_matrix& chip, reads_vector& control){
	set<string> ToDelete;
	//get all chromosomes that are not in the genome directory
	for (unsigned int i=0;i<control.size();i++){
			for (reads::iterator it = control[i].begin();it!=control[i].end();++it){
				string chr = path+"/"+ (*it).first+".fa";
				if (!file_exist(chr))
					ToDelete.insert((*it).first);
			}
		}

	for (unsigned int i=0;i<chip.size();i++){
		for (unsigned int j=0;j<chip[i].size();j++){
			for(reads::iterator it=chip[i][j].begin();it!=chip[i][j].end();++it){
				string chr = path+"/"+ (*it).first+".fa";
				if (!file_exist(chr))
					ToDelete.insert((*it).first);
			}
		}
	}

	for (set<string>::iterator it = ToDelete.begin();it!=ToDelete.end();++it){
		cerr<<"Warning: "<<*it<<" can not be found on genome directory, it will be ignored in further analysis"<<endl;
		for (unsigned int i=0;i<control.size();i++){
			control[i].erase(*it);
		}
	}

	for (set<string>::iterator it = ToDelete.begin();it!=ToDelete.end();++it){
		for (unsigned int i=0;i<chip.size();i++){
				for (unsigned int j=0;j<chip[i].size();j++){
					string chr = *it;
					chip[i][j].erase(chr);
				}
		}

	}

	ToDelete.clear();


	//this will work in the case of two conditions. A generalized implementation is needed

	vector<vector<string> > v,v2;

	for (int i=0;i<conditions_count;i++){
		v.push_back(vector<string>());
	}

	for (int i=0;i<conditions_count;i++){
		for (reads::iterator it= control[i].begin(); it!=control[i].end();++it)
			v[i].push_back((*it).first);
	}



	// get intersection between control files
	vector<string>::iterator it;
	int sizeToCreate=v[0].size()+v[1].size();
	vector<string> temp1(sizeToCreate,""),temp2(sizeToCreate,"");
	sort(v[0].begin(),v[0].end());
	sort(v[1].begin(),v[1].end());
	it = set_intersection(v[0].begin(),v[0].end(),v[1].begin(),v[1].end(),temp1.begin());
	temp1.resize(it-temp1.begin());



	// get intersection between chip files
	int index=0;
	for (int i=0;i<chip.size();i++){
		for (int j=0;j<chip[i].size();j++){
			v2.push_back(vector<string>());
		}
	}

	for (int i=0;i<chip.size();i++){
			for (int j=0;j<chip[i].size();j++){
				for (reads::iterator it=chip[i][j].begin();it!=chip[i][j].end();++it){
					v2[index].push_back((*it).first);
				}
				index++;
			}
	}



	sort(v2[0].begin(),v2[0].end());
	sort(v2[1].begin(),v2[1].end());
	it = set_intersection(v2[0].begin(),v2[0].end(),v[1].begin(),v[1].end(),temp2.begin());
	temp2.resize(it-temp2.begin());

	for (int i=2;i<conditions_count;i++){
		sort(temp2.begin(),temp2.end());
		sort(v2[i].begin(),v2[i].end());
		set_intersection(temp2.begin(),temp2.end(),v2[i].begin(),v2[i].end(),temp2.begin());
		temp2.resize(it-temp2.begin());
	}


	// intersect between chip and control
	sort(temp1.begin(),temp1.end());
	sort(temp2.begin(),temp2.end());

	it = set_intersection(temp1.begin(),temp1.end(),temp2.begin(),temp2.end(),chr_list.begin());
	chr_list.resize(it-chr_list.begin());

	//remove chrM from the list:
	vector<string>::iterator iM = find(chr_list.begin(),chr_list.end(),"chrM");
	if (iM!=chr_list.end()){
        chr_list.erase(iM);
	}


	// delete any chromosome that is not in chr_list

	// loop over everything and check if they are in chr_list

	for (unsigned int i=0;i<control.size();i++){
		for (reads::iterator it = control[i].begin();it!=control[i].end();++it){
			vector<string>::iterator a = find(chr_list.begin(),chr_list.end(),(*it).first);
			if (a==chr_list.end()){
				ToDelete.insert((*it).first);
				cerr<<"warning: will delete "<<(*it).first<<" since it is not common in all data"<<endl;
			}

		}
	}

	for (unsigned int i=0;i<chip.size();i++){
		for (unsigned int j=0;j<chip[i].size();j++){
			for(reads::iterator it=chip[i][j].begin();it!=chip[i][j].end();++it){
				vector<string>::iterator a = find(chr_list.begin(),chr_list.end(),(*it).first);
				if (a==chr_list.end()){
					ToDelete.insert((*it).first);
					cerr<<"warning: will delete "<<(*it).first<<" since it is not common in all data"<<endl;
				}
			}
		}
	}


	for (set<string>::iterator it = ToDelete.begin();it!=ToDelete.end();++it){
			for (unsigned int i=0;i<control.size();i++){
				control[i].erase(*it);
			}
	}

	for (set<string>::iterator it = ToDelete.begin();it!=ToDelete.end();++it){
			for (unsigned int i=0;i<chip.size();i++){
					for (unsigned int j=0;j<chip[i].size();j++){
						string chr = *it;
						chip[i][j].erase(chr);
					}
			}

		}


	v.clear();
	return;
}

void ConditionsHandler::build_gc_profile(){
	int max_medain_f_length = median_f_length[0]>median_f_length[1]?median_f_length[0]:median_f_length[1];
	GC_profiler GC = GC_profiler(bin_size,max_medain_f_length);
	GC.calculate(chr_list,path);
	sampled_sizes = GC.get_sampled_sizes();
	sizes = GC.get_sizes();
	GC_profile = GC.get();
	GC.clear();

}


void ConditionsHandler::build_CNV_profile(reads_vector& control){
	CNCalculator CNV = CNCalculator(large_bin_size);
	CNV.read_gc_profile(gc_index);

	for(unsigned int i=0;i<control.size();i++){
		CNV.calculate(control[i]);
		CNVs.push_back(CNV.get_CN());
		CNV.clear();
	}
}

void ConditionsHandler::count_reads(reads_matrix& chip, reads_vector& control){

	/*
	 * This function will report read count in control and chip libraries
	 */

	//count control reads

	control_counts = vector<int>(control.size(),0);
	control_max_chr = vector<string>(control.size(),"");
	int max_count;
	for (unsigned int i=0;i<control.size();i++){
		max_count = 0;
		for(reads::iterator it=control[i].begin();it!=control[i].end();++it){
			if ((*it).second.size() > max_count)
				control_max_chr[i] = (*it).first;
			control_counts[i]+=(*it).second.size();
		}
	}

	//count chip reads
	for (unsigned int i = 0;i<chip.size();i++){
		chip_max_chr.push_back(vector<string>(chip[i].size(),""));
		chip_counts.push_back(vector<int>(chip[i].size(),0));

	}
	for (unsigned int i=0;i<chip.size();i++){
		for (unsigned int j=0;j<chip[i].size();j++){
			max_count = 0;
			for (reads::iterator it= chip[i][j].begin();it!=chip[i][j].end();++it){
				if ((*it).second.size()> max_count)
					chip_max_chr[i][j] = (*it).first;
				chip_counts[i][j]+=(*it).second.size();
			}
		}
	}

	for (unsigned int i=0;i<control.size();i++){
		cout<<"Control reads count in condition "<<i+1<<": "<<control_counts[i]<<endl;
	}



	for (unsigned int i=0;i<chip.size();i++){
			for (unsigned int j=0;j<chip[i].size();j++){
				cout<<"ChIP read count in condition "<<i+1<<" and replicate "<<j+1<<": "<<chip_counts[i][j]<<endl;
			}
	}
}

void ConditionsHandler::build_denities(reads_matrix& chip,reads_vector& control){

	/*
	 * This function will build densities  for all chip  data and control data. It will
	 * place control densities in a temp density_vector variable
	 *
	 */


	//build control densities

	for (unsigned int i=0;i<control.size();i++){
		DensityBuilder data_builder = DensityBuilder(sizes,sampled_sizes,bin_size,median_f_length[i],
								min_f_length[i],max_f_length[i],pairedEnd);
		cout<<"Building Density for control "<<i+1<<endl;
		data_builder.build_density(control[i]);
		temp_control.push_back(data_builder.get());
		data_builder.clear();
	}


	//build target densities
	//loop over conditions
	for (unsigned int i=0;i<chip.size();i++){
		int avg_min_f_length=0,avg_med_f_length=0,avg_max_f_length=0;
		density_vector temp;
		for(unsigned int j=0;j<chip[i].size();j++){ // loop over replicates
			cout<<"Building Density for ChIP "<<i+1<<" replicate "<<j+1<<endl;
			DensityBuilder data_builder = DensityBuilder(sizes,sampled_sizes,bin_size,median_f_length[i],
													min_f_length[i],max_f_length[i],pairedEnd);
			data_builder.build_density(chip[i][j]);

			temp.push_back(data_builder.get());

			//data_builder.clear();
		}

		target.push_back(temp);
		temp.clear();
	}

}


void ConditionsHandler::normalize_single_condition(density_vector& target,density& control,density& CNV,
		vector<int>& chip_count,int control_count,int condition){


	/*
	 * This function will normalize all replicates of a condition
	 *
	 */
	int replicates = target.size();
	cout<<"I am at condition "<<condition+1<<" with "<<replicates<<" replicates"<<endl;
	Normalizer condition_normalizer = Normalizer(bin_size,large_bin_size);
	vector<counts> peaks = vector<counts>(replicates);
	condition_normalizer.normalize_copy_number(control,CNV);
	condition_normalizer.remove_blacklist(black_list,control);


	for (int i=0;i<replicates;i++){
		condition_normalizer.normalize_copy_number(target[i],CNV);
		if (!useSpikesCounts) //only if there is no spike-ins:
            condition_normalizer.normalize_library_size(target[i],chip_count[i],control_count);
		condition_normalizer.remove_blacklist(black_list,target[i]);
	}




	for (int i=0;i<replicates;i++){
		peaks[i] = get_peaks(target[i],control);
	}



	for (int i=0;i<replicates;i++)
		this->control[condition].push_back(control);


	// normalize the first replicate
	condition_normalizer.normalize_GC(target[0],this->control[condition][0],GC_profile,peaks[0],true);
	noise_ratio[condition].push_back(condition_normalizer.get_noise_ratio());
	expected_signal[condition].push_back(condition_normalizer.get_expected_signal());
	for (int i=1;i<replicates;i++){
		condition_normalizer.normalize_GC(target[i],this->control[condition][i],GC_profile,peaks[i],false);
		noise_ratio[condition].push_back(condition_normalizer.get_noise_ratio());
		expected_signal[condition].push_back(condition_normalizer.get_expected_signal());
	}
	peaks.clear();
}


void ConditionsHandler::normalize_all_independent(){


	for (int i=0;i<conditions_count;i++){
		control.push_back(density_vector());
		noise_ratio.push_back(vector<float>());
		expected_signal.push_back(vector<float>());
	}
	for (int i=0;i<conditions_count;i++){
		normalize_single_condition(target[i],temp_control[i],CNVs[i],chip_counts[i],control_counts[i],i);
	}

	for (int i=0;i<conditions_count;i++)
		temp_control[i].clear();
}


void ConditionsHandler::profile(reads_matrix& chip, reads_vector& control){
	conditions_count = chip.size();
	for (int i=0;i<chip.size();i++)
		total_counts+=chip[i].size();
	clean_reads(chip,control);
	count_reads(chip,control);

	if (pairedEnd){
		estimate_frag_lengths(chip);
	}


	/*some step calculations*/
	int max_length = max_f_length[0]>max_f_length[1]?max_f_length[0]:max_f_length[1];
	step = max_length/bin_size+1;


	build_gc_profile();
	build_CNV_profile(control); //will remove some chromosomes for which there is no GC-content profile available
	//clean now	again
	clean_reads(chip,control);

	build_denities(chip,control); //target & temp_control

	//clear reads, is it good? I need memory!
	//********************
	chip.clear();
	control.clear();
	//**********************

	build_gc_profile();  //HAITHAM!! WHY DO YOU CALL THIS FUNCTION TWICE?? HERE AND 7 LINES ABOVE!!!
	normalize_all_independent();



}


void ConditionsHandler::estimate_frag_lengths(reads_matrix& chip){ // estimate fragment lengths from chip data.
	//FragLengthCalculator c = FragLengthCalculator();
	int avg_min_f_length,avg_med_f_length,avg_max_f_length;
	for (int i =0;i<chip.size();i++){
		avg_min_f_length = avg_med_f_length = avg_max_f_length =0;
		for (int j=0;j<chip[i].size();j++){
			string chrom = chip_max_chr[i][j];
			FragLengthCalculator c = FragLengthCalculator();
			vector<int> dist  = c.getFragmentLengthDist(chip[i][j][chrom]);
			avg_min_f_length += dist[0];
			avg_med_f_length += dist[1];
			avg_max_f_length += dist[2];

		}
		max_f_length[i] = avg_max_f_length/chip[i].size();
		median_f_length[i] = avg_med_f_length/chip[i].size();
		min_f_length[i] = avg_min_f_length/chip[i].size();
	}
}

counts ConditionsHandler::get_peaks(density& target,density& control){
	// call peaks without generating obeservations for one replicate only
	density::iterator chr_it;
	float max_dense,pvalue,m,v,p,r;
	string chr;
	counts peaks;

	pvalue =1;
	for (chr_it=target.begin();chr_it!=target.end();++chr_it){
		chr = (*chr_it).first;
		peaks[chr] = vector<int>((*chr_it).second.size(),0);

		if (negbinom){
			m = estimate_mean(target[chr]);
			v = estimate_variance(target[chr],m);


			if (v<m){
				v=m;
			}
			p = m/v;
			r = m*m/(v-m);
		}

		for (unsigned int i=0;i<(*chr_it).second.size();i++){
			if (target[chr][i]!=-1){
				max_dense = control[chr][i]>0 ? control[chr][i]:1;

				if (negbinom){
					pvalue = gsl_cdf_negative_binomial_Q(round_to_int(target[chr][i]),p,r);
				}
				else
					pvalue = alglib::poissoncdistribution(round_to_int(target[chr][i])-1, max_dense);
				if (pvalue<threshold)
					peaks[(*chr_it).first][i] =1;
				}
				else
					peaks[(*chr_it).first][i] =-1;
		}
	}


	return peaks;
}


void ConditionsHandler::remove_singeltons(counts& peaks){
	counts::iterator chr_it;
	for(chr_it=peaks.begin();chr_it!=peaks.end();++chr_it){
		for(unsigned int i =2;i<(*chr_it).second.size()-2;i++)
			if(peaks[(*chr_it).first][i]==1 && peaks[(*chr_it).first][i-1]==0  &&
					peaks[(*chr_it).first][i+1]==0){
					peaks[(*chr_it).first][i]=0;
			}

	}
}

void ConditionsHandler::merge_peaks(counts& peaks){
	counts::iterator chr_it;
	for(chr_it=peaks.begin();chr_it!=peaks.end();++chr_it){
		int i=0;
		int size = (*chr_it).second.size();
		while(i<size-1){
			if((*chr_it).second[i]==1 && (*chr_it).second[i+1]==0){
				int count =0;
				int point =i+1;
				while(point<size &&(*chr_it).second[point]==0 ){
					count++;
					point++;
				}
				if (count<=min_dist){
					for(int j=i+1;j<point;j++)
						(*chr_it).second[j]=1;
				}
				i=point;
			}
			else
				i++;
		}
	}
}

void ConditionsHandler::generate_observations(){
	observation_seq temp;
	int observation,i,j,k=0,size;//,min_obs_size;//,rows;
	bool new_line;
	float max_dense;
	string chr;






	// initialize the maps and calculate the sizes
	// rows are total number of conditions; in our case it is 2
	// cols are number of replicates in each condition

	rows = target.size();
	for (int i=0;i<target.size();i++){
		cols.push_back(target[i].size());
	}

	int min_max_f = max_f_length[0]<max_f_length[1]?max_f_length[0]:max_f_length[1];
	min_obs_size = total_counts*(min_max_f/bin_size+1);

	vector<counts> peaks;

	for (i=0;i<rows;i++){
		for(int j=0;j<cols[i];j++){
			peaks.push_back(get_peaks(target[i][j],control[i][j]));
		}
	}

	for (i=0;i<peaks.size();i++){
		remove_singeltons(peaks[i]);
		merge_peaks(peaks[i]);
	}

	//initialize peaks final sum
	for (vector<string>::iterator chr_it=chr_list.begin();chr_it!=chr_list.end();++chr_it){
		size = sampled_sizes[(*chr_it)];
		chr = *chr_it;
		new_line = true;
		temp.chr = chr;
		peaks_final_sum[chr] = vector<int>(size,0);


		for (k=0;k<size;k++){
			int index = 0;
			for (i=0;i<rows;i++){
				for (j=0;j<cols[i];j++){
					peaks_final_sum[chr][k] += peaks[index][chr][k];
					index++;
				}
			}
		}

		for (k=0;k<size;k++){
			if(peaks_final_sum[chr][k]>0){
				for(i=0;i<rows;i++){
					for(j=0;j<cols[i];j++){
						observation = round_to_int(target[i][j][chr][k]-control[i][j][chr][k]);
						temp.values.push_back(observation);
						new_line = false;

					}
				}
			}
			else{
				if (!new_line){
					temp.start = k-temp.values.size()/total_counts;
					new_line=true;
					if (temp.values.size()>min_obs_size){
						obs.push_back(temp);

					}
					temp.values.clear();
				}


			}
		}

		temp.start = size-temp.values.size()/total_counts;
		new_line=true;
		if (temp.values.size()>min_obs_size){
			obs.push_back(temp);

		}
		temp.values.clear();



	}
}

void ConditionsHandler::inter_condition_normalization_spikein(vector<vector<int> > &SpikeReadNumbers){

    //normalize each replicate separatelly..

    vector<float> maxSpikeInReadCount (SpikeReadNumbers[0].size(),0);
	for (unsigned int i=0;i<SpikeReadNumbers.size();i++)
        for(unsigned int j=0;j<SpikeReadNumbers[i].size();j++)
            if (SpikeReadNumbers[i][j]>maxSpikeInReadCount[j])
                maxSpikeInReadCount[j] = SpikeReadNumbers[i][j]; //max per replicate, ignore conditions

	vector<float> scalingFactors;

	for (unsigned int i=0;i<SpikeReadNumbers.size();i++) {
        for(unsigned int j=0;j<SpikeReadNumbers[i].size();j++) {
            float factor  = maxSpikeInReadCount[j]/SpikeReadNumbers[i][j];
            scalingFactors.push_back(factor);
            cout<<"Scaling factor for condition " << i+1<<" replicate "<<j+1<<" is "<< factor<<endl;
        }
    }


	int index;
	for (unsigned int i=0;i<obs.size();i++){
		for (unsigned int j=0;j<obs[i].values.size();j+=total_counts){
			for (unsigned int k=0;k<total_counts;k++){
				index = k+j;
				obs[i].values[index] =scalingFactors[k]*obs[i].values[index];
			}
		}
	}

    //enforce min value to be 1
    for (int i=0;i<obs.size();i++) {
        for (int j=0;j<obs[i].values.size();j++){
            if(obs[i].values[j]<1){
                obs[i].values[j]=1;
            }
        }
    }
}




void ConditionsHandler::inter_conditions_normalization_library_size(){

	int normalization_factor = *max_element(control_counts.begin(),control_counts.end());
	int counter = 0;
	vector<float> ratios (0,total_counts);

	for (unsigned int i=0;i<target.size();i++){
		cols.push_back(target[i].size());
		//total_counts+=target[i].size();
	}

	for (unsigned int i=0;i<control_counts.size();i++){
		int factor = 1.0*control_counts[i]/normalization_factor;
		ratios.push_back(factor);
	}




	for (unsigned int i=0;i<obs.size();i++){
		for (unsigned int j=0;j<obs[i].values.size();j+=total_counts){
			for (int k=0;k<rows;k++){
				counter = 0;
				int factor = ratios[k];
				for (int l=0;l<cols[k];l++){
					int index = j+counter;
					if (index>=obs[i].values.size()){
						cerr<<"Error: Something wrong is happening "<<index<<" "<<obs[i].values.size()<<" "<<
								counter<<endl;
						exit(1);
					}
					obs[i].values[index] *=factor;
					counter++;
				}

			}
		}
	}
}

void ConditionsHandler::estimate_reads_counts_at_region(vector<int>& reads_denisty,vector<int>& estimation){

	/*
	 * starting from the summit sum all independent read counts
	 */
	int max =0;
	int max_index=0;
	float sum=0;


	if (reads_denisty.empty()){
		return;
	}
	for (unsigned int i=0;i<reads_denisty.size();i++){
		if (reads_denisty[i]>max)
			max_index = i;
	}
	int index = max_index;
	do{
		sum+=reads_denisty[index];
		index-=step;
	}while(index>0);
	index = max_index+step;
	while(index<reads_denisty.size()){
		sum+=reads_denisty[index];
		index+=step;
	}
	estimation.push_back(sum);

}



void ConditionsHandler::inter_conditions_normalization_regression(){


	vector<vector<float> > read_counts;
	vector<float> c1,c2; //c1 is x coofcient c2 is the intecept
	int ref_index=0;
	int count = 0;
	float min_noise_ratio = 1;
	for (unsigned int i=0;i<noise_ratio.size();i++){
		for(unsigned int j=0;j<noise_ratio[i].size();j++){
			if (noise_ratio[i][j]<min_noise_ratio){
				ref_index = count;
				min_noise_ratio = noise_ratio[i][j];
			}
			count++;

		}
	}




	// count is the total datasets count
	for (int i=0;i<total_counts;i++){
		read_counts.push_back(vector<float>());
	}
	c1 = vector<float>(total_counts,0);
	c2 = vector<float>(total_counts,0);

	int window_count=0;
	vector<float> window(total_counts,0);




	for (unsigned int i=0;i<obs.size();i++){
		int obs_size = obs[i].values.size()/total_counts; // number of observations per obs sequence
		int index = 0;

		string chr = obs[i].chr;
		while(index<obs_size){
			int chr_index = obs[i].start+index;
			while(peaks_final_sum[chr][chr_index] == total_counts && index<obs_size ){
				for (int j=0;j<total_counts;j++){
					window[j]+=obs[i].values[index*total_counts+j];
					window_count++;
				}
				chr_index++;
				index++;
			}


			//estimate densitites in the peak as the average density //
			//TODO: use estimate count function
			if (window_count>1){
				for (int j=0;j<total_counts;j++){
					float avg_value = (window[j]/window_count>=1)?window[j]/window_count:0;
					read_counts[j].push_back(avg_value);
					window[j] = 0;
				}
			}

			window_count = 0;

			index++;
		}

	}

	//Regression part
		int npoints = read_counts[0].size();
		alglib::ae_int_t nvars = 1; // linear
		for (unsigned int i=0;i<total_counts;i++){
			if (i == ref_index){


				//C2 is x coeficcient and C1 us the intercept
				c1[i] = 0;
				c2 [i] = 1;
			}


			else{
				alglib::real_2d_array xy;
				xy.setlength(npoints,nvars+1);
				int pos =0;
				for (unsigned int j=0;j<read_counts[i].size();j++){

				if (read_counts[i].size()!=read_counts[ref_index].size()){
					cerr<<"Read counts are not the same "<<read_counts[i].size()<<"\t"<<read_counts[ref_index].size()<<endl;
					exit(1);
				}

				xy(pos,nvars) = read_counts[ref_index][j];
				xy(pos,nvars-1) = read_counts[i][j];
				pos++;
				}

				alglib::linearmodel lm;
				alglib::ae_int_t info;
				alglib::lrreport ar;
				lrbuild(xy,npoints,nvars,info,lm,ar);
				if (info != 1) {
								cerr << "Error in linear regression, code: " << info <<"\n";
								exit(1);
				}

				alglib::real_1d_array v;
				v.setlength(nvars+1);
				lrunpack(lm,v,nvars);


				//C2 is x coeficcient and C1 us the intercept
				c2[i] = v(0);
				c1[i] = v(1);
				cout<<"Dependency between function is: "<<c2[i]<<"x+"<<c1[i]<<endl;
			}


		}

		// reference is is always c1=1 c2=0
		//send to normalization function C1x+c2
		int index;
		for (unsigned int i=0;i<obs.size();i++){
			for (unsigned int j=0;j<obs[i].values.size();j+=total_counts){
				for (unsigned int k=0;k<total_counts;k++){
					index = k+j;
					obs[i].values[index] =c2[k]*obs[i].values[index]+c1[k];
				}
			}
		}


	//enforce min value to be 1
	for (int i=0;i<obs.size();i++){
		for (int j=0;j<obs[i].values.size();j++){
			if(obs[i].values[j]<1){
				obs[i].values[j]=1;
			}
		}
	}



}


void ConditionsHandler::inter_condition_normalization_sequence_depth(){
	float max_expected_signal = 0;
	vector<float> c;


	for (unsigned int i=0;i<expected_signal.size();i++)
			for(unsigned int j=0;j<expected_signal[i].size();j++)
				if (expected_signal[i][j]>max_expected_signal)
					max_expected_signal = expected_signal[i][j];




	for (unsigned int i=0;i<expected_signal.size();i++){
				for(unsigned int j=0;j<expected_signal[i].size();j++){
					float factor  = max_expected_signal/expected_signal[i][j];//noise_ratio[i][j]/min_noise_ratio;
					c.push_back(factor);
					cout<<"Scaling factor is "<< factor<<endl;
				}
		}


	int index;
	for (unsigned int i=0;i<obs.size();i++){
		for (unsigned int j=0;j<obs[i].values.size();j+=total_counts){
			for (unsigned int k=0;k<total_counts;k++){
				index = k+j;
				obs[i].values[index] =c[k]*obs[i].values[index];
			}
		}
	}



		//enforce min value to be 1
		for (int i=0;i<obs.size();i++){
			for (int j=0;j<obs[i].values.size();j++){
				if(obs[i].values[j]<1){
					obs[i].values[j]=1;
				}
			}
		}

}

bool ConditionsHandler::agree_on_state(string& chr, int index, int state){

	for(unsigned int i=0;i<peaks_final.size();i++){
		for (unsigned int j=0;j<peaks_final[i].size();j++){
			if (peaks_final[i][j][chr][index]!=state)
				return false;
		}
	}

	return true;
}


void ConditionsHandler::calculate_initial_dists(){

	vector<int> states;
	// get min and max obs
	for (unsigned int i=0;i<obs.size();i++){
		for (unsigned int j=0; j<obs[i].values.size();j++){
			if (obs[i].values[j]>max_obs)
				max_obs = obs[i].values[j];
			if (obs[i].values[j]<min_obs)
				min_obs = obs[i].values[j];
		}
	}

	// initialize distributions counts
		range = max_obs-min_obs+1;
		for (int i=0;i<3;i++){
			emissions_r.push_back(vector<float>(2,0));
			emissions_p.push_back(vector<float>(2,0));
		}


	vector<float> c1,c2;
	vector <float> fc, diff;
	vector<int> key;

	vector<vector<float> > no_diff_readcounts, c1_readcounts, c2_readcounts; 

	for (int i=0;i<2;i++){
		//for each state initialize the mariginal distribution NB parameters
		no_diff_readcounts.push_back(vector<float>());
		c1_readcounts.push_back(vector<float>());
		c2_readcounts.push_back(vector<float>());

	}

	for (unsigned int i=0;i<obs.size();i++){

		for(int k=0;k<obs[i].values.size();k+=total_counts){
			int index = 0;
			for (int j=0;j<cols[0];j++){
				c1.push_back(obs[i].values[k+index]);
				index++;
			}

			for (int j=0;j<cols[1];j++){
				c2.push_back(obs[i].values[k+index]);
				index++;
			}

			int current_state = 0;



			// Fold change approach
			// all-vs-all fold change
			for (int j=0;j<c1.size();j++){
				for (int l=0;l<c2.size();l++){
					fc.push_back((1.0*c1[j])/(c2[l] ));
					diff.push_back(abs(c1[j]-c2[l]));

				}
			}


			float median = get_median(fc);
			float median2 = get_median(diff);
			//obs[i].fold_change.push_back(median);
			if (median2>0){

				if (median>=fold_change){
					current_state = 1;
				}
				else if (median<=1.0/(fold_change)){
					current_state = 2;
				}
			}


			if (current_state==0){
				fill_read_counts(no_diff_readcounts,c1,c2);
			}

			else if (current_state == 1){
				fill_read_counts(c1_readcounts,c1,c2);
			}

			else{
				fill_read_counts(c2_readcounts,c1,c2);
			}

			//initialize emissions
			//key.clear();
			//for (int i=0;i<c1.size();i++)
			//	key.push_back(c1[i]);
			//for (int i=0;i<c2.size();i++)
			//	key.push_back(c2[i]);

			//if (emissions[current_state].find(key)!=emissions[current_state].end())
			//	emissions[current_state][key]++;
			//else
			//	emissions[current_state][key]=1;



			states.push_back(current_state);

			fc.clear();
			c1.clear();
			c2.clear();

		}

		// initiate transitions
		for (int j=0;j<states.size()-1;j++){
			if (states[j]>2 || states[j+1]>2){
				cerr<<"ERROR: "<<states[j]<<"\t"<<states[j+1]<<"\t"<<states.size()<<"\t"<<j<<endl;
				exit(1);
			}
			transitions[states[j]][states[j+1]]++;
		}
		states.clear();
	}

	estimate_NB_coef(0,no_diff_readcounts);
	estimate_NB_coef(1,c1_readcounts);
	estimate_NB_coef(2,c2_readcounts);



}


void ConditionsHandler::fill_read_counts(vector<vector<float> >& to_fill, std::vector<float>& c1, std::vector<float>& c2){
	for (int i=0;i<c1.size();i++){
		to_fill[0].push_back(c1[i]);
	}

	for (int i=0;i<c2.size();i++){
		to_fill[1].push_back(c2[i]);
	}


}


void ConditionsHandler::estimate_NB_coef(int state, vector<vector<float> >& read_count){

	for (int i=0;i<read_count.size();i++){
		float m = estimate_mean(read_count[i]);
		float v = estimate_variance(read_count[i],m);
		if (v<m){
			v=1.002*m;
		}
		float p = m/v;
		float r = m*m/(v-m);
		cout<<state<<"\t"<<i<<"\t"<<m<<"\t"<<v<<"\t"<<p<<"\t"<<r<<endl;
		
			
		emissions_p[state][i] = p;
		emissions_r[state][i] = r;
		
			
	}
}


vector<observation_seq>  ConditionsHandler::get_observations(){
	return obs;
}


vector<vector<float> > ConditionsHandler::get_emissions_r(){
	return emissions_r;
}

vector<vector<float> > ConditionsHandler::get_emissions_p(){
	return emissions_p;
}

table_2d ConditionsHandler::get_transitions(){
	return transitions;
}

void ConditionsHandler::normalize_conditions(vector<vector<int> > &SpikeReadNumbers){
	//inter_conditions_normalization_library_size();

	//inter_conditions_normalization_regression();

	if (SpikeReadNumbers.size()==0) //do not use Spike-ins
        inter_condition_normalization_sequence_depth();
    else //use Spike-ins
        inter_condition_normalization_spikein(SpikeReadNumbers);
}

int ConditionsHandler::get_min_obs(){
	return min_obs;
}

int ConditionsHandler::get_sample_counts(){
	return total_counts;
}
void ConditionsHandler::clear(){

}

int ConditionsHandler::get_range(){
	return range;
}


map<string,int> ConditionsHandler:: get_chrom_sizes(){
	return sizes;
}

int ConditionsHandler::get_max_length_from_conditions(){
	int max_length = max_f_length[0]>max_f_length[1]?max_f_length[0]:max_f_length[1];
	return max_length;
}


int ConditionsHandler::get_med_length_from_conditions(){
	int median_length =  median_f_length[0]<median_f_length[1]? median_f_length[0]:median_f_length[1];
	return median_length;
}
