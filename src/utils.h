#ifndef UTILS_H_
#define UTILS_H_

#include <vector>
#include <string>
#include <map>
#include <algorithm>
#include <fstream>
#include <iostream>
#include <sstream>
#include "types.h"
#include "segmentation.h"

#define NA -1
#define HMCANDIFF_VERSION 1.04



bool equivelant(DNA_fragment tag1, DNA_fragment tag2);
bool compare(DNA_fragment tag1, DNA_fragment tag2);
bool compare_2d_vector(std::vector<float> , std::vector<float>);
std::vector <std::string > ReadFasta(const char * file_name);
std::string Read_chr(const char * file_name);
float get_max_element(float * values, int, int);
//float get_median(const std::vector<float> &);
float calculate_area(float*, int ,int);
std::string long2str(long);
std::string float2str(float);
bool BedCompare(BedEntry a, BedEntry b);
bool BedStartCompare(BedEntry a, BedEntry b);
int iround (float n);
bool equivelant(DNA_fragment tag1, DNA_fragment tag2);
void print_version();
std::vector<std::string> read_files_list(std::string);
std::vector<int> read_commaSeparated_list(std::string);
bool file_exist(std::string& name);
int get_min_state(double state0, double state1, double state2);
float estimate_mean(std::vector<float>&);
float estimate_variance(std::vector<float>& , float);
double bh_fdr(std::vector<double>&, float);
unsigned int nChooseK(int n, int k);
#endif /* UTILS_H_ */
