/*************************************************************************
Copyright (c) 2015, Haitham ASHOOR

>>> SOURCE LICENSE >>>
This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation (www.fsf.org); either version 2 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

A copy of the GNU General Public License is available at
http://www.fsf.org/licensing/licenses

>>> END OF LICENSE >>>
*************************************************************************/

#include "Normalizer.h"
#include "segmentation.h"
using namespace std;


Normalizer::Normalizer(int bin_length, int large_bin_length){


	bin_size = bin_length;
	large_bin_size = large_bin_length;
	control_lambda = 0;
	chip_lambda=0;
	control_win_count = vector<int>(101,0);
	chip_win_count = vector<int>(101,0);
	control_gc_dist = vector<float>(101,0);
	target_gc_dist = vector<float>(101,0);
	noise_ratio = 0;
	expected_signal = 0;
}

Normalizer::~Normalizer() {
	// TODO Auto-generated destructor stub
}


void Normalizer::normalize_copy_number(density& data, density& medians){
	density::iterator chr_it;
	for (chr_it=data.begin();chr_it!=data.end();++chr_it){
		for(unsigned int i=0;i<(*chr_it).second.size();i++){

            if (medians[(*chr_it).first].size()>0) {
                unsigned int bin_index = (i*bin_size+bin_size)/large_bin_size;

                if (bin_index >= medians[(*chr_it).first].size()){
                    cerr<<"Something wrong in "<<(*chr_it).first<<"\t"<<i<<"\t"<<bin_index<<medians[(*chr_it).first].size()<<
                            "\t"<<(*chr_it).second.size()<<endl;
                    exit(1);
                }

                if (medians[(*chr_it).first][bin_index]>0.3){
                    (*chr_it).second[i] /= medians[(*chr_it).first][bin_index];
                }
            }
		}
	}
}

void Normalizer::normalize_GC(density& target,density& control,density& GC_profile, counts& peaks, bool normalize_control){


	if (normalize_control)
		make_GC_dist(0,control,GC_profile,control_gc_dist,control_win_count,peaks);
	make_GC_dist(1,target,GC_profile,target_gc_dist,chip_win_count,peaks);
	correct_GC(target,control,GC_profile);

}


void Normalizer::make_GC_dist(int control,density& profile,density& GC_profile,vector<float>& dist,
		vector<int>& windows_count,counts& peaks){

	density::iterator chr_it;
	vector<float> accum_density(101,0);
	vector<vector<float> > density_raw;
	float startum_windows=0,GC_stratum=0;
	string chr;

	for (int i=0;i<101;i++){
		dist[i] =0;
		windows_count[i]=0;
		density_raw.push_back(vector<float>());
	}

	for (chr_it = profile.begin();chr_it!=profile.end();++chr_it){
		chr = (*chr_it).first;
		for (unsigned int i =0;i<(*chr_it).second.size();i++){
			if (GC_profile[chr][i]!=-1 && (*chr_it).second[i]!=-1){
                if (isnan(GC_profile[chr][i])) {
                    cout << "..strange values for GC for chromosome "<<chr <<" of size "<<(*chr_it).second.size() <<"; window number "<<i<<"; values to round "<<GC_profile[chr][i]<< endl;
					GC_profile[chr][i]=-1;
                } else {
                    int GC_index  = iround(GC_profile[chr][i]);
                    if (GC_index < 0 || GC_index >100){
                        cout << "..strange values for GC for chromosome "<<chr <<" of size "<<(*chr_it).second.size() <<"; window number "<<i<<"; values to round "<<GC_profile[chr][i]<< endl;
                        cerr<<"Something is wrong with the GC-content file "<<GC_index<<"\t"<<(*chr_it).second[i]<<endl;
                        exit(1);
                    }

                    if (control == 0){
                        density_raw[GC_index].push_back((*chr_it).second[i]);
                    }
                    else if (peaks[chr][i]== 0){
                        density_raw[GC_index].push_back((*chr_it).second[i]);
                    }
				}
			}
		}
	}

	for (int i=0;i<101;i++){

		int ten_percent = density_raw[i].size()/10;
		sort(density_raw[i].begin(),density_raw[i].end());
		for (unsigned int j=ten_percent;j<density_raw[i].size()-ten_percent;j++){
			accum_density[i]+=density_raw[i][j];
			windows_count[i]++;
		}
	}



	density_raw.clear();
	for(int i=0;i<27;i++){
		GC_stratum+=accum_density[i];
		startum_windows+=windows_count[i];
	}

	for(int i=0;i<27;i++)
		dist[i] = GC_stratum/startum_windows;


	for(int i=28;i<35;i+=2){
		GC_stratum=(accum_density[i]+accum_density[i-1])/(windows_count[i]+windows_count[i-1]);
		dist[i] = dist[i-1] = GC_stratum;
	}


	for(int i=35;i<66;i++){
		dist[i]=accum_density[i]/windows_count[i];
	}



	for(int i = 66;i<75;i+=2){
		GC_stratum=(accum_density[i]+accum_density[i-1])/(windows_count[i]+windows_count[i-1]);
		dist[i] = dist[i-1] = GC_stratum;
	}

	GC_stratum=0;
	startum_windows=0;
	for(int i=75;i<101;i++){
		GC_stratum+=accum_density[i];
		startum_windows+=windows_count[i];
	}

	for(int i=75;i<101;i++)
		dist[i] = GC_stratum/startum_windows;

}

void Normalizer::correct_GC(density& target,density& control,density& GC_profile){

	/*
	 * Here we will correct for GC content and noise ratio
	 */


	density::iterator chr_it;
	float max_chip=0,max_control=0;
	int i ,chip_lower,chip_upper,control_lower,control_upper;





	for (int i=0;i<101;i++){
			if (target_gc_dist[i]>max_chip)
				max_chip = target_gc_dist[i];
	}

	for (int i=0;i<101;i++){
				if (control_gc_dist[i]>max_control)
					max_control = control_gc_dist[i];
	}


	i =0;
	while(target_gc_dist[i]<0.1*max_chip)
		i++;
	chip_lower =i;

	i=100;
	while(target_gc_dist[i]<0.1*max_chip)
		i--;
	chip_upper=i;

	i=0;
	while(control_gc_dist[i]<0.1*max_control)
		i++;
	control_lower =i;

	i=100;
	while(control_gc_dist[i]<0.1*max_control)
		i--;
	control_upper=i;

	int final_lower = max(control_lower,chip_lower);
	int final_upper = min(control_upper,chip_upper);


	int chip_total_windows=0,control_total_windows=0;
	for (int i=final_lower;i<=final_upper;i++){
		chip_total_windows+=chip_win_count[i];
		control_total_windows+=control_win_count[i];
	}


	// calculate the expectations
	chip_lambda = 0;
	control_lambda = 0;


	for (int i=final_lower;i<=final_upper;i++){
		chip_lambda+=target_gc_dist[i]*chip_win_count[i]/chip_total_windows;
		control_lambda+=control_gc_dist[i]*control_win_count[i]/control_total_windows;
	}


	cout<<"chip lambda is: "<<chip_lambda<<endl;
	cout<<"control lambda is "<<control_lambda<<endl;
	noise_ratio = chip_lambda/control_lambda;
	if (noise_ratio>1)
		noise_ratio=1;
	cout<<"noise ratio is: "<<noise_ratio<<endl;

    if (noise_ratio>0.97) {
        cout <<"..You have a lot of noise in this experiment.\nThe results may not be trustful"<<endl;
    }


	float chip_sum = 0;
	float control_sum = 0;


	int step = 1;

	float correctionFactor=chip_lambda;
	if (noise_ratio==1) {
        correctionFactor=control_lambda; // because we do not want to artificially increase noise in the control sample
	}

	for(chr_it=target.begin();chr_it!=target.end();++chr_it){
		for(unsigned int i=0;i<(*chr_it).second.size();i++){
			if ((*chr_it).second[i] !=-1 && GC_profile[(*chr_it).first][i]!=-1){
					int index = (int)(GC_profile[(*chr_it).first][i]*100);
					if (index>=final_lower && index<=final_upper && target_gc_dist[index]!=0){
							(*chr_it).second[i] = (*chr_it).second[i]*chip_lambda/target_gc_dist[index];

						}
					if (i%step == 0)
						chip_sum+=(*chr_it).second[i];
				}
		}
    }







	for(chr_it=control.begin();chr_it!=control.end();++chr_it){
		for(unsigned int i=0;i<(*chr_it).second.size();i++){
			if ((*chr_it).second[i] !=-1 && GC_profile[(*chr_it).first][i]!=-1){
				int index = (int)(GC_profile[(*chr_it).first][i]*100);
				if (index>=final_lower && index<=final_upper && control_gc_dist[index]!=0){

					(*chr_it).second[i] = (*chr_it).second[i]*correctionFactor/control_gc_dist[index];

				}
				else
					(*chr_it).second[i] = (*chr_it).second[i]*noise_ratio;
				if (i%step==0)
					control_sum+=(*chr_it).second[i];
			}


		}
	}

	expected_signal = chip_sum-control_sum;


}



void Normalizer::normalize_library_size(density& profile,int my_reads,int reference_reads ){
	density::iterator chr_it;
	vector<float>::iterator it;
	float reads_ratio = float(reference_reads)/my_reads;
	cout<<"Reads ratio is: "<<reads_ratio<<endl;
	for (chr_it=profile.begin();chr_it!=profile.end();++chr_it){
		for(it=(*chr_it).second.begin();it!=(*chr_it).second.end();++it)
			if ((*it) !=-1)
				(*it)*=reads_ratio;
	}
}

void Normalizer::remove_blacklist(vector<BedEntry>& blacklist,density& profile){
	vector<BedEntry>::iterator chr_it;
	for (chr_it = blacklist.begin();chr_it!=blacklist.end();++chr_it){
		string chr = (*chr_it).chr;
		if(profile.find(chr) == profile.end() || profile[chr].size()==0)
			continue;
		int start_point = (*chr_it).start;
		while((start_point+1)%bin_size!=0)
			start_point++;

		for (int i= start_point;i<(*chr_it).end;i+=bin_size){
		unsigned int index = (i+1)/bin_size-1;
			if(index >= profile[chr].size()){
				cerr<<"Blacklisted Value out of chromosome bounds. Exiting..."<<chr<<"\t"<<profile[chr].size()
						<<profile[chr].size()<<endl;
				exit(1);
			}

			profile[chr][index] = -1;
		}

	}

}



float Normalizer::get_noise_ratio(){


	return noise_ratio;
}

float Normalizer::get_expected_signal(){
	return expected_signal;
}

void Normalizer::clear(){

	target_gc_dist.clear();
	control_gc_dist.clear();
	chip_win_count.clear();
	control_win_count.clear();


}
