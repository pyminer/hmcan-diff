/*************************************************************************
Copyright (c) 2015, Haitham ASHOOR

>>> SOURCE LICENSE >>>
This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation (www.fsf.org); either version 2 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

A copy of the GNU General Public License is available at
http://www.fsf.org/licensing/licenses

>>> END OF LICENSE >>>
*************************************************************************/

#include "GCprofiler.h"
#include "utils.h"
#include <fstream>

using namespace std;

GC_profiler::GC_profiler(int bin,int fragment) {
	fragment_length = fragment;
	bin_length = bin;

}

GC_profiler::~GC_profiler() {
}

inline float GC_profiler::calculate_GC(string& chr_seq,int start,int length){
	float G=0,C=0;
	if (length<=0){
		return -1;
	}
	for(int j =start;j<start+length;j++)
		if (chr_seq[j]!='N'){
			if (chr_seq[j] =='G' || chr_seq[j] == 'g')
				G++;
			if (chr_seq[j] == 'C' || chr_seq[j] == 'c')
				C++;
		//count++;
		} else {
			return -1;
		}
		return (G+C)/length;
}

void GC_profiler::calculate_GC_sampled_bins(string chr, string& chr_seq,int size){
	int chr_size = chr_seq.size();
	for(int i=0;i<size;i++){
		int position = i*bin_length+bin_length;
		int start = position-fragment_length < 0 ? 0:position-fragment_length;
		int length = position+fragment_length < chr_size? 2*fragment_length:chr_size-position+1;
		float GC = calculate_GC(chr_seq,start,length);
		if (isnan(GC)) {
            cerr << "Something is wrong with GC-content count for density bins; chromosome "<<chr<<" bin "<<i << " position around "<<position<<endl;
            GC=-1;
		}
		sampled_GC[chr][i] = GC;
	}
}


void GC_profiler::calculate(vector<string>& list, string path){



	for (vector<string>::iterator it=list.begin();it!=list.end();++it){
		string chr = *it;
		string chromosome_path = path+"/"+ chr+".fa";
		string chrom = Read_chr(chromosome_path.c_str());
		int size = chrom.size();
		int sampled_size = size/bin_length+1;
		sampled_GC[chr] = vector<float>(sampled_size,0);
		sampled_sizes[chr] = sampled_size;
		sizes[chr] = size;
		calculate_GC_sampled_bins(chr,chrom,sampled_size);

	}
}


density GC_profiler::get(){
	return sampled_GC;
}


void GC_profiler::print(string filename){
	ofstream out;
	out.open(filename.c_str());
	if(!out){
		cerr<<"Can not open the file: "<<filename<<endl;
		exit(1);
	}

	for (density::iterator it=sampled_GC.begin();it!=sampled_GC.end();++it){
		out<<(*it).first<<"\t"<<(*it).second.size();
		for (vector<float>::iterator itt = (*it).second.begin();itt!=(*it).second.end();++it)
			out<<*itt<<endl;
	}
}

void GC_profiler::load_from_file(ifstream& infile, ifstream& chr_lengths){
	string chr;
	int length;

	while(true){
		if (chr_lengths.eof())
			break;
		cin>>chr>>length;
		sizes[chr] = length;
		sampled_sizes[chr] = length/bin_length+1;
	}

	while(true){
		if (infile.eof())
			break;
		cin>>chr>>length;
		if (length != sampled_sizes[chr]){
			cerr<<"Chromosome sizes do not match, please provide correct GC profile and chromosome lengths file"<<endl;
			exit(1);
		}
		sampled_GC[chr] = vector<float>(length,0);

		for (int i=0;i<length;i++){
			cin>>sampled_GC[chr][i];
		}
	}

}
void GC_profiler::clear(){
	for (map<string, vector<float> >::iterator chr_it = sampled_GC.begin();chr_it!=sampled_GC.end();++chr_it)
		(*chr_it).second.clear();
	sampled_GC.clear();
	sizes.clear();
	sampled_sizes.clear();
}

map<string,int> GC_profiler::get_sizes(){
	return sizes;
}

map<string,int> GC_profiler::get_sampled_sizes(){
	return sampled_sizes;
}
