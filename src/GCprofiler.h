/*
 * GCprofiler.h
 *
 *  Created on: Apr 30, 2014
 *      Author: haitham
 */

#ifndef GCPROFILER_H_
#define GCPROFILER_H_


#include <vector>
#include <map>
#include <string>
#include "types.h"

class GC_profiler {
public:
	GC_profiler(int,int);
	virtual ~GC_profiler();
	void load_from_file(std::ifstream&,std::ifstream&);
	void print(std::string filename);
	void calculate(std::vector<std::string>&, std::string);
	std::map<std::string,int> get_sizes();
	std::map<std::string,int> get_sampled_sizes();
	void clear();
	density get();
private:
	int bin_length,fragment_length;
	std::map<std::string,int> sampled_sizes,sizes;
	density sampled_GC;
	void calculate_GC_sampled_bins(std::string,std::string&,int);
	inline float calculate_GC(std::string&,int,int);

};

#endif /* GCPROFILER_H_ */
