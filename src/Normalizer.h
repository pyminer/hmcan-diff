/*
 * Normalizer.h
 *
 *  Created on: May 3, 2014
 *      Author: haitham
 */

#ifndef NORMALIZER_H_
#define NORMALIZER_H_


#include <map>
#include <string>
#include <vector>

#include "utils.h"
#include "types.h"

class Normalizer {
public:
	Normalizer(int,int);
	virtual ~Normalizer();
	void normalize_copy_number(density&,density&);
	void normalize_GC(density&,density&,density&,counts&, bool);
	void normalize_library_size(density&,int,int);
	void print_wig(std::string);
	void remove_blacklist(std::vector<BedEntry>& , density&);
	float get_noise_ratio();
	float get_expected_signal();
	void clear();

private:
	//counts peaks;
	std::vector<float> target_gc_dist, control_gc_dist;
	std::vector<int> chip_win_count,control_win_count;
	int large_bin_size,bin_size;
	float chip_lambda,control_lambda,noise_ratio,expected_signal;
	void make_GC_dist(int control,std::map<std::string,std::vector<float> >& ,
			std::map<std::string,std::vector<float> >& ,std::vector<float>&,std::vector<int>&, counts&);
	void correct_GC(std::map<std::string,std::vector<float> >&,std::map<std::string,std::vector<float> >&,
			std::map<std::string,std::vector<float> >& );

};

#endif /* NORMALIZER_H_ */
