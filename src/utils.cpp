/*************************************************************************
Copyright (c) 2013, Haitham ASHOOR

>>> SOURCE LICENSE >>>
This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation (www.fsf.org); either version 2 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

A copy of the GNU General Public License is available at
http://www.fsf.org/licensing/licenses

>>> END OF LICENSE >>>
*************************************************************************/

#include "utils.h"
#include <iomanip>
using namespace std;


#include <sys/stat.h>

bool compare(DNA_fragment tag1, DNA_fragment tag2){
	return tag1.start<tag2.start;
}

bool equivelant(DNA_fragment tag1, DNA_fragment tag2){
	return tag1.start == tag2.start;
}

vector <string > ReadFasta(const char * file_name){

	ifstream infile;

	string str,temp;
	vector <std::string > seqs;
	infile.open(file_name);
	if (!infile){
		std::cout<<"unable to open the file "<<file_name<<std::endl;
		exit(1);
	}

	do {
		getline(infile,temp);
		if (infile.eof())
				break;
		if (temp[0] == '>')
		{
			if (!str.empty()){

				transform(str.begin(), str.end(),str.begin(), ::toupper);
				seqs.push_back(str);
				str.clear();// = '';
			}
		}
		else if (temp[0] != '>' && !temp.empty())
		{

			str = str+temp;
			temp.clear();

		}





	}while(true);
	if (!str.empty())
			seqs.push_back(str);
	return seqs;
}



string Read_chr(const char * file_name){
	string chr,line;
	vector <string> lines;
	ifstream infile;
	infile.open(file_name);
	if (!infile){
		cout<<"Can not open file "<<file_name<<endl;
		exit(1);
	}
	chr = "";
	getline(infile,line);

	while(getline(infile,line)){
		lines.push_back(line);
	}

	for(unsigned int i=0;i<lines.size();i++)
		chr.append(lines[i]);
	return chr;
}


float get_max_element(float * values, int start, int end){
	float max = values[start];
	for (int i=1;i<end;i++)
		if(values[i]>max)
			max = values[i];
	return max;
}



float calculate_area(float *values, int start,int end){ // calculate area under curve using trapozoidal rule
	float norm_factor = 1/(2*(end-start));
	float area = norm_factor*(values[start]+values[end]);
	for(int i=start+1;i<end;i++)
		area+=2*norm_factor*values[i];
	return area;
}



string long2str(long number){
	stringstream ss;//create a stringstream
	ss << number;//add number to the stream
	return ss.str();//return a string with the contents of the stream
}

string float2str(float number){
	stringstream ss;//create a stringstream
	ss << number;//add number to the stream
	return ss.str();//return a string with the contents of the stream
}


bool compare_2d_vector(vector<float> a,vector<float> b){
	return a[0]<b[0];
}


bool BedCompare(BedEntry a, BedEntry b){
	return a.score>b.score;
}

bool BedStartCompare(BedEntry a, BedEntry b){
	return a.start<b.start;
}



int iround(float n){
	n = n*100;
	if (int(n) - n >0.5)
		return (int) n +1;
	else
		return (int) n;



}

void print_version()
{
	cout<<"HMCan-diff V" << HMCANDIFF_VERSION<< " is a program to identify differential regions from histone "<<
				"modifications in ChIP-seq cancer samples" <<endl;
}

std::vector<int> read_commaSeparated_list(std::string str) {
    vector<int> intList;

    std::stringstream ss(str);

    int i;

    while (ss >> i)
    {
        intList.push_back(i);

        if (ss.peek() == ',')
            ss.ignore();
    }

	return intList;
}


vector<string> read_files_list(string list){
	vector<string> FilesList;
	string File;
	ifstream Files;
	Files.open(list.c_str()); // read TargetFiles list

	if (!Files){
		cerr<<"Error: Can not open the file: "<<list<<endl;
		exit(1);
	}

	do{
		Files>>File;
		if (Files.eof())
			break;
		FilesList.push_back(File);
		} while(1);

	return FilesList;
}

bool file_exist(string& name){
  struct stat buffer;
  return (stat (name.c_str(), &buffer) == 0);
}


int get_min_state(double double_side, double left_side, double right_side){
	double min=double_side;
	int min_index=0;


	if (left_side < min){
		min = left_side;
		min_index = 1;
	}

	if (right_side < min){
		min = right_side;
		min_index = 2;
	}


	return min_index;
}

float estimate_mean(vector<float>& values){
	float mean = 0;
	int count = 0;
	for (int i=0;i<values.size();i++)
		if (values[i]!=-1){
			mean+=round_to_int(values[i]);
			count++;
		}

	return mean/count;
}


float estimate_variance(vector<float>& values, float mean){
	float variance = 0;
	int count=0;
	for (int i=0;i<values.size();i++){
		if (values[i]!=-1){
			variance += (round_to_int(values[i])-mean)*(round_to_int(values[i])-mean);
			count++;
		}
	}
	return variance/count;
}



double bh_fdr(std::vector<double>& input_pval, float alpha) {
    std::sort(input_pval.begin(), input_pval.end(), std::greater<double>());
    double m = input_pval.size();
    int k = input_pval.size();
    for (vector<double>::iterator pval = input_pval.begin();pval!=input_pval.end();++pval) {

        if (*(pval) <= (double)k/m * alpha) {
            cerr << endl << "all values below " << *(pval) << " are significant. index (1-based): " << k << endl;
            return *(pval);
        }
        k--; //Decrease rank
    }
    return -1;
}


unsigned int nChooseK(int n, int k){
    if (k > n) return 0;
    if (k * 2 > n) k = n-k;
    if (k == 0) return 1;

    unsigned int result = n;
    for( int i = 2; i <= k; ++i ) {
        result *= (n-i+1);
        result /= i;
    }
    return result;
}
