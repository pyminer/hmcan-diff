/*************************************************************************
Copyright (c) 2015, Haitham ASHOOR

>>> SOURCE LICENSE >>>
This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation (www.fsf.org); either version 2 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

A copy of the GNU General Public License is available at
http://www.fsf.org/licensing/licenses

>>> END OF LICENSE >>>
*************************************************************************/


#include "Hmm.h"
#include <iostream>
#include <limits>
#include <cstdlib>
#include <cmath>
using namespace std;

#include "utils.h"
#include <gsl/gsl_randist.h>
Hmm::Hmm(vector<observation_seq>& _obs,table_2d& _transitions,table_2d& _emissions_p, table_2d& _emissions_r,int _states ,int _replicates,
		int _min_obs, float threshold, int _range, int _c1, int _c2):
			obs(_obs),transitions(_transitions),emissions_p(_emissions_p), emissions_r(_emissions_r){

	min_obs =_min_obs;
	states=_states;
	replicates = _replicates;
	initial_dist = vector<float>(states,0);
	posterior_threshold = threshold;
	range = _range;
	pseudo_count =1;
	//zero_probability = vector<float>(states,0);
	C1 = _c1;
	C2 = _c2;
	marginals = vector<float>(states,0);	
	//estimate_emissions();
	estimate_transitions();

}

Hmm::~Hmm() {
	// TODO Auto-generated destructor stub
}

void Hmm::forward_backward(int col, table_2d & post){

	table_2d alphas, betas;
	double sum,sum1;
	int  alpha_index ,beta_index;
	unsigned int obs_count=obs[col].values.size();
	alpha_index = 0;
	beta_index = obs_count/replicates-2;
	int i;


	float smoothing_value = 1.0/states;
	for(i=0;i<obs_count;i+=replicates){

		alphas.push_back(vector<float> (states,0));
		betas.push_back(vector<float> (states,smoothing_value)); //make sure, 0.5 should be 1/states
	}


	// filling the first element of alphas
	for (int i=0;i<states;i++){
		alphas[0][i] = initial_dist[i]*get_joint_prob(col,0,i);
	}

	alpha_index++;
	//forward algorithm
	for (i=replicates;i<obs[col].values.size();i+=replicates){
		sum1 = 0;
		for(int j=0;j<states;j++){
			sum = 0;
			for(int k=0;k<states;k++){
				sum+=alphas[alpha_index-1][k]*transitions[k][j];
			}
			alphas[alpha_index][j] = get_joint_prob(col,i,j)*sum;
			sum1+=alphas[alpha_index][j];
		}
		for (int j=0;j<states;j++)
			alphas[alpha_index][j]/=sum1;
		alpha_index++;

	}


	//backward algorithm
	for(i=obs[col].values.size()-states*replicates;i>=0;i-=replicates){
		sum1=0;
		for(int j=0;j<states;j++){
			sum=0;
			for (int k=0;k<states;k++){
				sum+=betas[beta_index+1][k]*transitions[j][k]*get_joint_prob(col,i,k);
			}
			betas[beta_index][j] = sum;
			sum1+=betas[beta_index][j];
		}

		for (int j=0;j<states;j++)
			betas[beta_index][j]/=sum1;
		beta_index--;

	}


	//calculate posterior probability
	for(i=0;i<alphas.size();i++){
		sum1= 0;
		for(int j=0;j<states;j++){
			post[col][i*states+j] = alphas[i][j]*betas[i][j];
			sum1+=post[col][i*states+j];
		}

		for(int j=0;j<states;j++){
			post[col][i*states+j]/=sum1;
			//just to avoid log problems
			if (post[col][i*states+j] > 0.99999)
				post[col][i*states+j] = 0.99999;
			else if (post[col][i*states+j]<1e-16)
				post[col][i*states+j] = 1e-16;
		}
	}


}

inline float Hmm::get_joint_prob(int col, int i,int state){


	
	float joint_p = 1;
	int c;

	for (int j=0;j<replicates;j++){
		int k =  obs[col].values[i+j];
		if (j<C1)
			c=0;
		else
			c=1;
		joint_p *=  gsl_ran_negative_binomial_pdf (k, emissions_p[state][c], emissions_r[state][c]);//emissions[state][j][key];
		
	}

	//joint_p = gsl_ran_negative_binomial_pdf (c1_sum/C1, emissions_p[state][0], emissions_r[state][0])*gsl_ran_negative_binomial_pdf (c2_sum/C2, emissions_p[state][1], emissions_r[state][1]);

	return joint_p;
}


void Hmm::decode(int col,table_2d_i& decoded_states,table_2d& post){



	int obs_count = obs[col].values.size()/replicates;
	for (int i=0;i<obs_count;i++){
		for (int j=1;j<states;j++){
			if (post[col][i*states+j]>posterior_threshold){
				decoded_states[col][i] = j;
			}
		}
	}


}


void Hmm::estimate_emissions(){
	/*
	int sum;
	for (unsigned int i=0;i<emissions.size();i++){
		for (unsigned int j=0;j<emissions[i].size();j++){
			sum = 0;
			for (unsigned int k=0;k<emissions[i][j].size();k++)
				sum+=emissions[i][j][k];
			for (unsigned int k=0;k<emissions[i][j].size();k++)
				emissions[i][j][k]/=sum;

		}	
	}*/
	return;
}





void Hmm::estimate_transitions(){
	vector<float> marginals(states,0);
	float total = 0;
	unsigned int i,j;

	for (i=0;i<transitions.size();i++){
		for(j=0;j<transitions[i].size();j++){
			marginals[i]+=transitions[i][j];
			total+=transitions[i][j];
		}


		for (j=0;j<transitions[i].size();j++)
			transitions[i][j]/=marginals[i];
	}


	for (int i=0;i<states;i++){
		initial_dist[i] = marginals[i]/total;
	}



}



void Hmm::re_estimate_transitions(table_2d_i& decoded_states){
	unsigned int i,j;

	for(i=0;i<transitions.size();i++)
		for(j=0;j<transitions[i].size();j++)
			transitions[i][j]=0;

	for (i=0;i<decoded_states.size();i++)
		for (j=0;j<decoded_states[i].size()-1;j++)
			transitions[decoded_states[i][j]][decoded_states[i][j+1]]++;

	estimate_transitions();

}

void Hmm::re_estimate_emissions(table_2d_i& decoded_states){
	cout<<"Re-adjusting parameters..."<<endl;
	unsigned int i,j;

	table_2d no_diff_readcounts, c1_readcounts, c2_readcounts; 
	vector<int> key;

	for (int i=0;i<2;i++){
		//for each state initialize the mariginal distribution NB parameters
		no_diff_readcounts.push_back(vector<float>());
		c1_readcounts.push_back(vector<float>());
		c2_readcounts.push_back(vector<float>());

	}
	


	for (i=0;i<decoded_states.size();i++){
			for (j=0;j<decoded_states[i].size();j++){
				int state =  decoded_states[i][j];
				for (int k=0;k<replicates;k++){
					
					key.push_back(obs[i].values[j*replicates+k]);	
				}

				if (state==0)
						fill_read_counts(no_diff_readcounts,key);
					else if (state==1)
						fill_read_counts(c1_readcounts,key);
					else
						fill_read_counts(c2_readcounts,key);
				key.clear();

				
			}
	}

	//estimate_emissions();
	estimate_NB_coef(0,no_diff_readcounts);
	estimate_NB_coef(1,c1_readcounts);
	estimate_NB_coef(2,c2_readcounts);
	cout<<"Done"<<endl;
}



void Hmm::posterior_decoding_iteration(table_2d_i& decoded_states,table_2d& post){

	unsigned int i;
	for (i=0;i<obs.size();i++){

		forward_backward(i,post);
		decode(i,decoded_states,post);
	}
}

void Hmm::adjust_probs(vector<vector<int> >& decoded_states){
	re_estimate_transitions(decoded_states);
	re_estimate_emissions(decoded_states);
}


vector<float> Hmm::get_initial_dists(){
	return initial_dist;
}

vector<float> Hmm::get_zero_probabilites(){
	vector<float> z =vector <float>(2,0);
	return z;
}


void Hmm::fill_read_counts(table_2d& to_fill, vector<int>& key){
	for (unsigned int i=0;i<key.size();i++){
		if (i<C1)
			to_fill[0].push_back(key[i]);
		else
			to_fill[1].push_back(key[i]);
	}
}

void Hmm::estimate_NB_coef(int state, table_2d& read_count){

	for (int i=0;i<read_count.size();i++){
		float m = estimate_mean(read_count[i]);
		float v = estimate_variance(read_count[i],m);
		if (v<m){
			v=1.02*m;
		}
		
		float p = m/v;
		float r = m*m/(v-m);
		cout<<state<<"\t"<<i<<"\t"<<m<<"\t"<<v<<"\t"<<p<<"\t"<<r<<endl;	
		emissions_p[state][i] = p;
		emissions_r[state][i] = r;
		
		
			
	}
}







