/*
 * PeakCaller.h
 *
 *  Created on: May 6, 2014
 *      Author: haitham
 */

#ifndef PEAKCALLER_H_
#define PEAKCALLER_H_

#define my_states 3 //we have only states in the peak calling process.

#include <vector>
#include <string>
#include "utils.h"

class PeakCaller {
public:
	PeakCaller(std::string ,std::vector<observation_seq>& , int , int , int ,float , int, int, int, int,std::string& , std::string&);
	virtual ~PeakCaller();
	void call_peaks( int, int,int);
	//void print_posterior(std::map<std::string,int>&);
	void print_wig(std::map<std::string,int>&);
	void print();

private:
	table_2d_i decoded_states;
	//table_2d post;
	std::vector<observation_seq>& obs;
	//table_2d& transitions;
	//std::vector<std::vector<float> >& emissions_r, emissions_p;
	//std::vector<float> initial_dist,zero_probability;


	std::vector<BedEntry>  peaks,regions;
	std::vector<int> peaks_count;
	int peak_max,step,bin_size,replicates,fragment_length,C1_rep,C2_rep; //the step is equal to 2*fragment length;
	float final_threshold, fold_change;
	std::string name, C1_label, C2_label;

	//void get_peaks_learn(int);
	float score_peak(int,int,int, int, float&);
	void get_peaks_final(std::string chr, int, int );
	void merge_peaks(int);

	//inline float get_joint_prob(std::vector<int>&, int);
	//float get_log_fc(int,int,int);
	int estimate_read_count(std::vector<int>&);


};

#endif /* PEAKCALLER_H_ */
