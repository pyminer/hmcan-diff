/*************************************************************************
Copyright (c) 2015, Haitham ASHOOR

>>> SOURCE LICENSE >>>
This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation (www.fsf.org); either version 2 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

A copy of the GNU General Public License is available at
http://www.fsf.org/licensing/licenses

>>> END OF LICENSE >>>
*************************************************************************/

#include "CNCalculator.h"
#include "segmentation.h"



using namespace std;


CNCalculator::CNCalculator(int bin_size) {
	// TODO Auto-generated constructor stub
	ploidy = 4; //to cover all cases; should not affect negatively cases with 2,3s
	large_bin_size = bin_size;
}

CNCalculator::~CNCalculator() {
	// TODO Auto-generated destructor stub
}


void CNCalculator::read_gc_profile(string& name){
	ifstream gc_profile;
	gc_profile.open(name.c_str());
	string line;
	while(getline(gc_profile,line)){
		vector<string> tokens;
		istringstream stream(line);
		copy(istream_iterator<string>(stream),
		     istream_iterator<string>(),
			 back_inserter<vector<string> >(tokens));
		if (tokens.size()<5){
			cerr<<"Warning: the line: "<<line<<" ,does not follow format. Line will be ignored"<<endl;
                        continue;
		}
		string key = tokens[0];
		//string key = tokens[0];
		float content = atof(tokens[2].c_str());
		float mapability = atof(tokens[4].c_str());
		GC_profile[key].push_back(content);
		notNprofile[key].push_back(mapability);

    }
}

void CNCalculator::clean_reads(reads& data,reads& control){

	reads::iterator chr_it;
	set<string> ToDelete;

	for (chr_it=data.begin();chr_it!=data.end();++chr_it){
		if(GC_profile.find((*chr_it).first) == GC_profile.end()){
			ToDelete.insert((*chr_it).first);
			cerr<<"Warning: "<<(*chr_it).first<<
					" can not be found on GC index, it will be ignored in further analysis"<<endl;
		}
	}

	for (chr_it=control.begin();chr_it!=control.end();++chr_it){
		if(GC_profile.find((*chr_it).first) == GC_profile.end()){
			ToDelete.insert((*chr_it).first);
			cerr<<"Warning: "<<(*chr_it).first<<
					" can not be found on GC index, it will be ignored in further analysis"<<endl;
		}
	}

	for (set<string>::iterator it=ToDelete.begin();it!=ToDelete.end();++it){
		data.erase((*it));
		control.erase((*it));
	}

	ToDelete.clear();
	for(chr_it=control.begin();chr_it!=control.end();++chr_it)
		if(data.find((*chr_it).first)==data.end()){
			ToDelete.insert((*chr_it).first);
	}

	for (set<string>::iterator it=ToDelete.begin();it!=ToDelete.end();++it){
			control.erase((*it));
	}

	return;
}

void CNCalculator:: calculateCopyNumberMedians() {
    density::iterator it;
    for ( it=ratio_profile.begin() ; it != ratio_profile.end(); it++ ) {
            int breakPointStart = 0;
            int breakPointEnd;
            float median;
            int length_ =  (*it).second.size();
            string chrom = (*it).first;
            breakpoints[chrom].push_back(length_-1);

            vector <float>medianProfile_ (length_,-1);

            for (int i  = 0; i < (int)breakpoints[chrom].size();i++) {
                breakPointEnd =  breakpoints[chrom][i];
                vector<float> data;
                int notNA = 0;
                for (int j = breakPointStart; j <= breakPointEnd; j++)
                    if (ratio_profile[chrom][j] != -1) {
                        data.push_back(ratio_profile[chrom][j]);
                        notNA++;
                    }
                if(data.size()==0){
                	median = NA;

                } else
                	median = get_median(data); //including the last point



                for (int j = breakPointStart; j<= breakPointEnd; j++) {
                    medianProfile_[j] = median;
                }

                breakPointStart = breakPointEnd+1;
                data.clear();
            }
            medians[chrom] = medianProfile_;
            medianProfile_.clear();
    	}
}

int CNCalculator::recalculateRatioUsingCG () {

	 map<string,vector <float> >::iterator it;
	 bool intercept = 1;
	 float minExpectedGC = .35;
	 float maxExpectedGC = .55;
	 float minMappabilityPerWindow = 0.6;
	 //try degree 3 and 4, SELECT THE ONE WITH LESS ITTERATIONS:
	 int minDegreeToTest = 3;
	 int maxDegreeToTest = 4;
	 int selectedDegree=minDegreeToTest;
	 int maximalNumberOfIterations = 200;
	 int bestNumberOfIterations=maximalNumberOfIterations;
	 int maximalNumberOfCopies = ploidy*2;
	 float interval = float (0.01) ;
	 vector <float> y; //y ~ ax^2+bx+c
	 vector <float> x;


	 //fill x and y:

	  for ( it=read_count.begin() ; it != read_count.end(); it++ ) {
		  if (! (((*it).first).find("X")!=string::npos || ((*it).first).find("Y")!=string::npos)){ //??
			  for (unsigned int i = 0; i< (*it).second.size(); i++) {
				  float value = (*it).second.at(i);
	              float nonN = notNprofile[(*it).first].at(i);
	              float GC = GC_profile[(*it).first].at(i);
	              if ((value>0)&&(nonN>minMappabilityPerWindow)) {
	            	  x.push_back(GC);
	                  y.push_back(value/nonN);
	               }
			  }
	        }
	     }

	    for (int degree=minDegreeToTest;degree<=maxDegreeToTest; degree++) {

	    	//first guess about parameters
	    	int npoints = degree+2;

	    	double around [MAXDEGREE+2];
	    	for (int i = 0; i<npoints; i++) {
	    		around[i] = minExpectedGC + (maxExpectedGC-minExpectedGC)/(npoints-1)*i; //0.55-0.35
	    	}  // for degree = 3 one will get : { 0.35, 0.40, 0.45, 0.5, 0.55 }; for 0.35 and 0.55


	    	double yValues [MAXDEGREE+2];
	    	for (int i = 0; i <npoints; i++)
	    		yValues[i] = calculateMedianAround(interval, float(around[i]));
	    	alglib::ae_int_t nvars = degree; //fit by cubic polynomial
	    	alglib::real_2d_array xy;
	    	xy.setlength(npoints,nvars+1);
	    	for (int i = 0; i <npoints; i++) {
	    		xy(i,degree) = yValues[i];
	    		xy(i,degree-1) = around[i];
	    		for (int j = degree-2; j>=0; j--) {
	    			xy(i,j)=xy(i,j+1)*around[i];
			}

			/* this is equal to
			xy(i,0) = around[i]*around[i]*around[i];
			xy(i,1) = around[i]*around[i];
			xy(i,2) = around[i];
			xy(i,3) = yValues[i]; */
	    	}
	    	alglib::linearmodel lm;
	    	alglib::ae_int_t info;
	    	alglib::lrreport ar;

	    	if  (intercept)
	    		lrbuild(xy,npoints,nvars,info,lm,ar);
	    	else
	    		lrbuildz(xy,npoints,nvars,info,lm,ar);

	    	if (info != 1) {
			cerr << "Error in the first linear regression (the first guess about parameters), code: " << info <<"\n";
	    	}
	    	alglib::real_1d_array v;
	    	v.setlength(nvars+int(intercept));
	    	lrunpack(lm,v,nvars);
	    	double a[MAXDEGREE+1];
	    	for (int i = 0; i <degree; i++) {
				a[i] = v(i);
	    	}

	    	if  (intercept)
				a[degree] = v(degree);
			else
				a[degree] = 0;

	        int realNumberOfIterations = maximalNumberOfIterations;
	    	float rmserror = runEM(x,y,a,degree,realNumberOfIterations,ploidy,maximalNumberOfCopies, intercept);
	    	if (rmserror == -1) {
	    		cerr << "Error in EM => unable to calculate normalized profile\n";
	    		return NA;
	    	}
		//cout << "root mean square error = " << rmserror << "\n";
		if (realNumberOfIterations<bestNumberOfIterations) {
	            selectedDegree = degree;
	            bestNumberOfIterations=realNumberOfIterations;
	        }
	    	cout << "Y = ";
	    	for (int i=0; i<degree;i++) {
	        	cout << a[i]<<"*x^" <<  degree-i <<"+" ;
		}
	    	cout << a[degree] <<"\n";

	    }


	    int degree = selectedDegree;
	    const int npoints = degree+2;

	    double around [MAXDEGREE+2];
	    for (int i = 0; i<npoints; i++) {
			around[i] = minExpectedGC + (maxExpectedGC-minExpectedGC)/(npoints-1)*i; //0.55-0.35
	    }  // for degree = 3 one will get : { 0.35, 0.40, 0.45, 0.5, 0.55 }; for 0.35 and 0.55


	    double yValues [MAXDEGREE+2];
	    for (int i = 0; i <npoints; i++)
			yValues[i] = calculateMedianAround(interval, float(around[i]));
	    alglib::ae_int_t nvars = degree; //fit by cubic polynomial
	    alglib::real_2d_array xy;
	    xy.setlength(npoints,nvars+1);
		for (int i = 0; i <npoints; i++) {
			xy(i,degree) = yValues[i];
			xy(i,degree-1) = around[i];
			for (int j = degree-2; j>=0; j--) {
				xy(i,j)=xy(i,j+1)*around[i];
			}
			/* this is equal to
			xy(i,0) = around[i]*around[i]*around[i];
			xy(i,1) = around[i]*around[i];
			xy(i,2) = around[i];
			xy(i,3) = yValues[i]; */
		}
		alglib::linearmodel lm;
		alglib::ae_int_t info;
		alglib::lrreport ar;

	   	if  (intercept)
	        	lrbuild(xy,npoints,nvars,info,lm,ar);
		else
			lrbuildz(xy,npoints,nvars,info,lm,ar);
		if (info != 1) {
			cerr << "Error in the first linear regression (the first guess about parameters), code: " << info <<"\n";
		}
		alglib::real_1d_array v;
	   	v.setlength(nvars+int(intercept));
		lrunpack(lm,v,nvars);
		double a[MAXDEGREE+1];
		for (int i = 0; i <degree; i++) {
				a[i] = v(i);
		}

	    	if  (intercept)
			a[degree] = v(degree);
		else
			a[degree] = 0;

	        bestNumberOfIterations = maximalNumberOfIterations;
	    	float rmserror = runEM(x,y,a,degree,bestNumberOfIterations,ploidy,maximalNumberOfCopies, intercept);
		if (rmserror == -1) {
			cerr << "Error in EM => unable to calculate normalized profile\n";
			return NA;
		}
		//cout << "root mean square error = " << rmserror << "\n";
	    	cout << "Y = ";
	    	for (int i=0; i<degree;i++) {
	        	cout << a[i]<<"*x^" <<  degree-i <<"+" ;
		}
	    	cout << a[degree] <<"\n";


	    for ( it=read_count.begin() ; it != read_count.end(); it++ ) {
	        if (ratio_profile[(*it).first].size()!=((*it).second).size())
	            ratio_profile[(*it).first].resize(((*it).second).size());
	        for (unsigned int i = 0; i< (*it).second.size(); i++) {
	            float x = GC_profile[(*it).first].at(i);
	            float value = (*it).second.at(i);
	            float nonN = notNprofile[(*it).first].at(i);
	            float rati;
	            if ((x>0)&&(nonN>minMappabilityPerWindow))
	                rati =value/polynomial(x,a,1,degree)/nonN;
	            else{

	                rati = NA;
	            }
	            if ((rati != NA)&&(rati < 0))
	                rati = 0; //this happens if  polynomial(x,a,b,c,d) < 0
	            ratio_profile[(*it).first].at(i) =rati ;
	        }
	    }
	    x.clear();
	    y.clear();
	    return bestNumberOfIterations;

}



double CNCalculator::calculateMedianAround (float interval, float around ) {

	float maxCG = around+interval;
		float minCG = around-interval;

		vector <float> myValuesAround;
	    map<string,vector <float> >::iterator it;
		for ( it=read_count.begin() ; it != read_count.end(); it++ ) {
	        if (! (((*it).first).find("X")!=string::npos || ((*it).first).find("Y")!=string::npos))
				for (unsigned int i = 0; i< (*it).second.size(); i++) {
	                float GC = GC_profile[(*it).first].at(i);
					if ((  GC<=maxCG)&&(GC>=minCG)) {
	                    float value = (*it).second.at(i);
						if (value>0) //non-zero values
							myValuesAround.push_back(value);
	                }
				}
		}
	    if (myValuesAround.size()==0) {
	        cerr << "Error: zero reads in windows with the GC-content around "<<around<< " with interval "<< interval<<", will try again with "<< interval*4<<"\n";
	        cerr << "Unable to proceed..\n";
	        cerr << "Try to rerun the program with higher number of reads\n";
	        exit(-1);

	    }
		float median = get_median(myValuesAround);
		myValuesAround.clear();
		return median;
}

void CNCalculator::calculateBreakpoints() {
	double breakPointThreshold = 0.8;
	int breakPointType = 2;
	cout << "..Calculating breakpoints, breakPointThreshold = " <<breakPointThreshold<<"\n";
	map<string,vector <float> >::iterator it;
	for ( it=ratio_profile.begin() ; it != ratio_profile.end(); it++ ) {
		cout << "..processing chromosome " <<(*it).first<<"\n";
		int result = calculateBreakpoints_general(breakPointThreshold,(*it).second.size(),(*it).second,breakpoints[(*it).first],0,breakPointType);
		if (result == 0) {
			cerr << "..failed to run segmentation on chr" << (*it).first << "\n";
		}
	}
}


void CNCalculator::calculate(std::map<std::string,std::vector<DNA_fragment > >& tags){
	int read_counts = 0,total_chrs = 0;
	for (reads::iterator chr_it = tags.begin();chr_it!=tags.end();++chr_it){
		if (GC_profile.find((*chr_it).first) == GC_profile.end()){
			cerr<<"Warning: Chromosome "<<(*chr_it).first<<" is not in your large bins file. It will be ignored in further analysis"<<endl;
			tags.erase(chr_it); // remove the data not contained in the copy number file
			continue;
		}
		else
			total_chrs++;
		read_counts+=(*chr_it).second.size();
		int bins_count =GC_profile[(*chr_it).first].size();
		read_count[(*chr_it).first] = vector<float>(bins_count,0);
		for(vector<DNA_fragment>::iterator v = (*chr_it).second.begin();v!=(*chr_it).second.end();++v){
			int bin_index = (*v).start/large_bin_size;
			if (bin_index >= bins_count){
			cerr<<"Error: " << "Coordinates for provided "<<(*chr_it).first << " and the one in the large bins file does not match,"
					" please check your large bins file!"<<bin_index<<"\t"<<bins_count<<"\t"<<(*v).start<<"\t"<<
					large_bin_size<<endl;
				exit(1);
			}
			read_count[(*chr_it).first][bin_index]+=1;
		}
	}
		cout<<"Total reads count read by FREEC is: "<<read_counts<<endl;
		recalculateRatioUsingCG ( );
		calculateBreakpoints();
		calculateCopyNumberMedians();
}

map <string, vector <float> > CNCalculator:: get_CN(){
	return medians;

}

void CNCalculator::clear(){
	for (density::iterator chr_it=GC_profile.begin();chr_it!=GC_profile.end();++chr_it){
		//(*chr_it).second.clear();
		read_count[(*chr_it).first].clear();
		ratio_profile[(*chr_it).first].clear();
		medians[(*chr_it).first].clear();
		//notNprofile[(*chr_it).first].clear();
	}

	for (counts::iterator chr_it=breakpoints.begin();chr_it!=breakpoints.end();++chr_it){
		(*chr_it).second.clear();
	}

	read_count.clear();
	ratio_profile.clear();
	//GC_profile.clear();
	medians.clear();
	//notNprofile.clear();
	breakpoints.clear();
}


void CNCalculator::print(ofstream& out){
	for (density::iterator chr_it = medians.begin();chr_it!=medians.end();++chr_it){
		for(unsigned int i= 0;i<(*chr_it).second.size();i++){
			int start = i*large_bin_size;
			out<<(*chr_it).first<<"\t"<<start<<"\t"<<(*chr_it).second[i]<<endl;
		}
	}
}
