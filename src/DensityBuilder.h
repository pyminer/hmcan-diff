/*
 * DensityBuilder.h
 *
 *  Created on: May 2, 2014
 *      Author: haitham
 */

#ifndef DENSITYBUILDER_H_
#define DENSITYBUILDER_H_

#include <map>
#include <string>
#include <vector>

#include "types.h"
#include "utils.h"

class DensityBuilder {
public:
	DensityBuilder(std::map<std::string,int>& ,std::map<std::string,int>&,int,int,int,int,bool);
	virtual ~DensityBuilder();
	void build_density(reads&);
	std::vector<int> getLengthDist();
	density get();
	void clear();
private:
	std::map<std::string,int>& sampled_sizes,sizes;
	density profile_density;
	int med,left,right,bin_length;
	float h,slope1,slope2,b1,b2,left_area,right_area;
	bool pairedEnd;
	void build_single_profile(std::vector<DNA_fragment >& ,std::string);
	void extend_reads(std::vector<DNA_fragment >&,int);
	int getMaxLength();
	void calculateFragLengthDist(std::vector<DNA_fragment >& );
	void getFragmentLengthDistr(std::vector<int>&);
	void calculateDensityCoef();

};

#endif /* DENSITYBUILDER_H_ */
