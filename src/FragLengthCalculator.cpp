/*
 * FragLengthCalculator.cpp
 *
 *  Created on: May 21, 2017
 *      Author: ashoorhm
 */

#include "FragLengthCalculator.h"

#include <algorithm>
#include <iterator>
#include <set>
#include <cmath>
#include <cfloat>
#include <limits>
#include <iostream>
using namespace std;


FragLengthCalculator::FragLengthCalculator() {
	// TODO Auto-generated constructor stub
	left = 0;
	right = 0;
	med = 0;

}

FragLengthCalculator::~FragLengthCalculator() {
	// TODO Auto-generated destructor stub
}



void FragLengthCalculator::ComputeFragLengthDist(vector<int>& tags){
	vector<int> data (tags);
	int ndatapoints = data.size();
	if (ndatapoints==0) {
		cerr << "Error: zero values to calculate medians..\n";
		exit(-1);
	}
	sort(data.begin(),data.end());
	// Get median
	med = ndatapoints % 2 == 1 ? data[(ndatapoints-1)/2] : (data[ndatapoints/2 - 1] + data[ndatapoints/2])/float(2.0);
	int upperboundary=med*10;
	int maxIndexToKeep = 0;
	for (; maxIndexToKeep<ndatapoints;maxIndexToKeep++) {
		if(data[maxIndexToKeep]>upperboundary) {
			break;
		}
	}
	data.resize(maxIndexToKeep);
	right=data[ceil(float((maxIndexToKeep-1)*99.0)/100)];
	left=data[ceil(float((maxIndexToKeep-1)*5.0)/100)];

	data.clear();

	return ;

}


vector<int> FragLengthCalculator::getFragmentLengthDist(vector<DNA_fragment>& tags){
	vector<int> dist, fragmentLengths;
	for(vector<DNA_fragment>::iterator tag_it = tags.begin(); tag_it!=tags.end();++tag_it)
		if ((*tag_it).fragmentLength >0) {
			fragmentLengths.push_back((*tag_it).fragmentLength);
		}
	ComputeFragLengthDist(fragmentLengths);
	cout << "Evaluated fragment sizes: "<<left<<", " << med << ", "<< right <<endl;
	dist.push_back(left);
	dist.push_back(med);
	dist.push_back(right);

	return dist;
}
