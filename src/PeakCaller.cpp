/*************************************************************************
Copyright (c) 2015, Haitham ASHOOR

>>> SOURCE LICENSE >>>
This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation (www.fsf.org); either version 2 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

A copy of the GNU General Public License is available at
http://www.fsf.org/licensing/licenses

>>> END OF LICENSE >>>
*************************************************************************/

#include "PeakCaller.h"
//#include "Hmm.h"
#include <iomanip>
#include <cmath>
#include <map>
#include <sstream>
using namespace std;

#include <gsl/gsl_randist.h>
PeakCaller::PeakCaller(string _name,vector<observation_seq>& _obs, int _step, int _bin_size, 
		int _replicates,float _final_threshold, int _fragment_length, int c1, int c2, int _fold_change,
		string& C1_label, string& C2_label):
				obs(_obs){
	
	//learn_threshold = _learn_threshold;
	//final_threshold = _final_threshold;
	final_threshold = -10*log(_final_threshold);
	peak_max =0;
	bin_size = _bin_size;
	name = _name;
	replicates = _replicates;
	step = replicates*_step; //my_states*_step;
	peaks_count = vector<int>(my_states-1,0);
	fragment_length = _fragment_length;
	fold_change = _fold_change;

	C1_rep = c1;
	C2_rep = c2;

	this->C1_label = C1_label;
	this->C2_label = C2_label;

}

PeakCaller::~PeakCaller() {
	// TODO Auto-generated destructor stub
}



int PeakCaller::estimate_read_count(vector<int>& density){
	int max_idx=0;
	int max = density[0];
	int n = density.size();
	int read_count = 0;
	for (int i=1;i<n;i++){
		if (density[i]>max){
			max=density[i];
			max_idx = i;
		}
	}

	read_count+=density[max_idx];
	int idx=max_idx+step;

	while(idx<n){
		read_count+=density[idx];
		idx+=step;
	}

	idx = max_idx-step;
	while(idx>0){
		read_count+=density[idx];
		idx-=step;
	}

	return read_count;

}

float PeakCaller::score_peak(int start,int end,int obs_index, int condition, float& log_fc){
	float score = 0;
	int i,j;

	if (start <0 || end<0){
		cerr<<"Something wrong: "<<start<<"\t"<<end<<endl;
		exit(1);
	}
	
	if (end<=start)
		return -1;


	//Calculate the score for each peak as log10 p-value
	//pvalue is calculated using as described in MAnorm 
	vector<int> c1_reads, c2_reads;

	for (int i=start;i<=end;i++){
		int c1_sum=0, c2_sum=0;
		for (int j=0;j<replicates;j++){
			if(j<C1_rep)
				c1_sum +=obs[obs_index].values[i*replicates+j];
			else
				c2_sum +=obs[obs_index].values[i*replicates+j];
		}
		c1_reads.push_back(c1_sum);
		c2_reads.push_back(c2_sum);

	}

	

	int c1_read_count = estimate_read_count(c1_reads);
	int c2_read_count = estimate_read_count(c2_reads);

	int total_reads = c1_read_count+c2_read_count;
	log_fc = log2(1.0*c1_read_count/c2_read_count);
	if (total_reads<20){
		double p1 = -10*log(nChooseK(total_reads,c1_read_count)*pow(2,(-1.0*(total_reads+1))));
		double p2 = -10*log(nChooseK(total_reads,c2_read_count)*pow(2,(-1.0*(total_reads+1))));
		return min(p1,p2);

	}

	else{
		double log_p = total_reads*log(total_reads)-c1_read_count*log(c1_read_count)-c2_read_count*log(c2_read_count)-(total_reads+1)*log(2.0);
		if (log_p<-500)
			log_p=-500;
		return -10*log_p;
	}

	return score;
}



void PeakCaller::call_peaks( int merge_distance, int max_iter, int min_obs){

	//
	for (unsigned int i=0;i<obs.size();i++){
		decoded_states.push_back(vector<int>(obs[i].values.size()/replicates,0)); // set states to the default state.
		//post.push_back(vector<float>(obs[i].values.size()/replicates*my_states));
	}
	for (int i=0;i<obs.size();i++){
		for (int j=0;j<obs[i].values.size();j+=replicates){
			int C1_sum = 0;
			int C2_sum = 0;
			for (int k=0;k<replicates;k++){
				if (k<C1_rep)
					C1_sum+=obs[i].values[j+k];
				else
					C2_sum+=obs[i].values[j+k];
			}

			int loci = j/replicates;
			float fc = 1.0*C1_sum/C2_sum;
			float diff = abs(C1_sum-C2_sum);
			if (diff>5){
				if (fc>fold_change){
					decoded_states[i][loci] =1;
				}
				else if (fc<1.0/fold_change){
					decoded_states[i][loci] =2;
				}
				else{
					decoded_states[i][loci] =0;
				}


			}
			else{
				decoded_states[i][loci]=0;
			}
		}
	}


	for (unsigned int i=0;i<obs.size();i++){
		get_peaks_final(obs[i].chr,obs[i].start,i);
	}

	//merge for broad peaks
	merge_peaks(merge_distance);

	cout<<"Done!"<<endl;
	print();

}


void PeakCaller::get_peaks_final(string chr, int chr_start, int obs_index){

	unsigned int i;
	int start=-1,end=-1;
	float score;
	BedEntry temp;

	for (int k=1;k<my_states;k++){
		if (decoded_states[obs_index][0] == k)
			start =0;

		for(i=0;i<decoded_states[obs_index].size()-1;i++){
			if (decoded_states[obs_index][i]!=k && decoded_states[obs_index][i+1]==k)
				start = i;
			else if (decoded_states[obs_index][i]==k && decoded_states[obs_index][i+1]!=k){
				end = i;
				float fc;
				score = score_peak(start,end,obs_index,k,fc);
				temp.chr = chr;
				temp.start = (start+chr_start)*bin_size-1;
				temp.end = (end+chr_start)*bin_size;
				temp.condition = k;
				temp.score = score;
				temp.logfc = fc;
				if (end<start){
					cerr<<"Something wrong is going on "<<end<<"\t"<<start<<endl;
				}
				if (start!=-1 && end!=-1)
					peaks.push_back(temp);
			}

		}


		if (decoded_states[obs_index][decoded_states[obs_index].size()-1]==k){

				end = decoded_states[obs_index].size()-1;
				float fc;
				score = score_peak(start,end,obs_index,k,fc);
				temp.chr = chr;
				temp.start = (start+1+chr_start)*bin_size-1;
				temp.end = (end+chr_start)*bin_size;
				temp.condition = k;
				temp.score = score;
				temp.logfc = fc;
				if (start!=-1 && end!=-1)
					peaks.push_back(temp);
		}
	}

}



void PeakCaller::merge_peaks(int dist){
	bed_map chrs;
	BedEntry temp;
	bed_map::iterator chr_it;
	for(vector<BedEntry>::iterator it = peaks.begin();it!=peaks.end();++it)
		chrs[(*it).chr].push_back((*it));
	bool pushed = false;
	for(chr_it=chrs.begin();chr_it!=chrs.end();++chr_it){
		sort((*chr_it).second.begin(),(*chr_it).second.end(),BedStartCompare);
		temp.chr = (*chr_it).first;
		temp.start = (*chr_it).second[0].start;
		temp.end = (*chr_it).second[0].end;
		temp.condition = (*chr_it).second[0].condition;
		temp.score = (*chr_it).second[0].score;
		temp.logfc = (*chr_it).second[0].logfc;

		for (unsigned int i=1;i<(*chr_it).second.size();i++){
			if ((*chr_it).second[i].start-temp.end+2<dist && (*chr_it).second[i].condition == temp.condition &&
					temp.score>final_threshold &&(*chr_it).second[i].score >final_threshold &&
							((*chr_it).second[i].end - (*chr_it).second[i].start)>fragment_length){

				//merge if peaks are close and from same condition and one of them at least significant
				temp.end = (*chr_it).second[i].end;
				temp.score = (*chr_it).second[i].score>temp.score ? (*chr_it).second[i].score:temp.score;
				temp.logfc = (*chr_it).second[i].logfc > temp.logfc ? (*chr_it).second[i].logfc:temp.logfc;
				pushed = false;
			}
			else{
				pushed = true;
				regions.push_back(temp);
				temp.start = (*chr_it).second[i].start;
				temp.end = (*chr_it).second[i].end;
				temp.score = (*chr_it).second[i].score;
				temp.condition = (*chr_it).second[i].condition;
				temp.logfc = (*chr_it).second[i].logfc;
			}
		}
	}
	if (!pushed)
			regions.push_back(temp);
}



void PeakCaller::print(){
	ofstream out;
	out.setf(ios::fixed);
	out.open((name+"_peaks.bed").c_str());


		for(unsigned int i=0;i<peaks.size();i++)
			
				if (peaks[i].score>final_threshold && (peaks[i].end-peaks[i].start)>fragment_length)
				if (peaks[i].condition==1)
					out<<peaks[i].chr<<'\t'<<peaks[i].start<<'\t'<<peaks[i].end<<'\t'<<"peak"<<i+1<<'\t'
										<<peaks[i].score<<"\t.\t"<<C1_label<<"\t"<<peaks[i].logfc<<endl;
				else
					out<<peaks[i].chr<<'\t'<<peaks[i].start<<'\t'<<peaks[i].end<<'\t'<<"peak"<<i+1<<'\t'
										<<peaks[i].score<<"\t.\t"<<C2_label<<"\t"<<peaks[i].logfc<<endl;

	out.close();




	out.open((name+"_regions.bed").c_str());
	for(unsigned int i=0;i<regions.size();i++){
			if (regions[i].score>final_threshold && (regions[i].end-regions[i].start)>fragment_length)
				if (regions[i].condition==1)
					out<<regions[i].chr<<'\t'<<regions[i].start<<'\t'<<regions[i].end<<'\t'<<"peak"<<i+1
					<<'\t'<<regions[i].score<<"\t.\t"<<C1_label<<"\t"<<regions[i].logfc<<endl;
				else
					out<<regions[i].chr<<'\t'<<regions[i].start<<'\t'<<regions[i].end<<'\t'<<"peak"<<i+1
									<<'\t'<<regions[i].score<<"\t.\t"<<C2_label<<"\t"<<regions[i].logfc<<endl;
	
	}
	out.close();

}

/*
void PeakCaller::print_posterior(map<string,int>& chrom_sizes){
	ofstream wig_file;
    wig_file.open((name+"_posterior_state0.wig").c_str());
    int start;
    string chr;
    unsigned int i,j;
    for(i=0;i<obs.size();i++){
    	start = obs[i].start*bin_size+1;
    	chr = obs[i].chr;
    	wig_file<<"fixedStep chrom="<<chr<<" start="<<start<<" step="<<bin_size<<endl;
    	for(j=0;j<post[i].size()-my_states;j+=my_states){
    		if (start < chrom_sizes[chr]-bin_size){
    			float p = post[i][j];
    			wig_file<<std::setprecision(6)<<p<<endl;
    		}
    		start+=bin_size;

    	}
    }
    wig_file.close();

    wig_file.open((name+"_posterior_state1.wig").c_str());
    for(i=0;i<obs.size();i++){
       	start = obs[i].start*bin_size+1;
       	chr = obs[i].chr;
       	wig_file<<"fixedStep chrom="<<chr<<" start="<<start<<" step="<<bin_size<<endl;
       	for(j=0;j<post[i].size()-my_states;j+=my_states){
       		if (start < chrom_sizes[chr]-bin_size){
       			float p = post[i][j+1];
       			wig_file<<std::setprecision(6)<<p<<endl;
       		}
       		start+=bin_size;

       	}
       }
     wig_file.close();


     wig_file.open((name+"_posterior_state2.wig").c_str());
     for(i=0;i<obs.size();i++){
        	start = obs[i].start*bin_size+1;
        	chr = obs[i].chr;
        	wig_file<<"fixedStep chrom="<<chr<<" start="<<start<<" step="<<bin_size<<endl;
        	for(j=0;j<post[i].size()-my_states;j+=my_states){
        		if (start < chrom_sizes[chr]-bin_size){
        			float p = post[i][j+2];
        			wig_file<<std::setprecision(6)<<p<<endl;
        		}
        		start+=bin_size;

        	}
        }
          wig_file.close();

}*/

void PeakCaller::print_wig(map<string,int>& chrom_sizes){
	vector<ofstream*> wig_files;
	int replicate_index=0;
	string filename;
	for (int i=0;i<replicates;i++){
		ofstream * temp = new ofstream();
		wig_files.push_back(temp);
	}
	for (int i=0;i<replicates;i++){
		stringstream ss;

		if (i<C1_rep){
			ss << name+"_" + C1_label +"_replicate_"<<replicate_index+1<<".wig";
			filename = ss.str();
			wig_files[i]->open(filename.c_str());
			replicate_index++;
		}

		if (i == C1_rep)
			replicate_index = 0;

		if (i>=C1_rep){
			ss << name+"_" + C2_label +"_replicate_"<<replicate_index+1<<".wig";
			filename = ss.str();
			wig_files[i]->open(filename.c_str());
			replicate_index++;
		}
	}
	for(unsigned int i=0;i<obs.size();i++){
		int start = obs[i].start*bin_size+1;
		string chr = obs[i].chr;
		for (int j=0;j<replicates;j++)
			*wig_files[j]<<"fixedStep chrom="<<chr<<" start="<<start<<" step="<<bin_size<<std::endl;
		for (unsigned int j=0;j<obs[i].values.size();j+=replicates){
			if (start < chrom_sizes[chr]-bin_size){
				for (int k=0;k<replicates;k++)
					*wig_files[k]<<obs[i].values[j+k]<<endl;
				start+=bin_size;
			}
		}
	}

	for(int i=0;i<replicates;i++){
		wig_files[i]->close();
		delete wig_files[i];
	}

}