/*
 * Hmm.h
 *
 *  Created on: May 4, 2014
 *      Author: haitham
 */

#ifndef HMM_H_
#define HMM_H_

#include <vector>
//#include "utils.h"
#include "types.h"

class Hmm {
public:
	Hmm(std::vector<observation_seq>&,table_2d&,table_2d& ,table_2d&, int,int, int, float,int, int , int);
	virtual ~Hmm();

	/*posterior decoding*/
	void posterior_decoding_iteration(std::vector<std::vector<int> >&,std::vector<std::vector<float> >&);
	void adjust_probs(std::vector<std::vector<int> >&);
	std::vector<float> get_initial_dists();
	std::vector<float> get_zero_probabilites();
	std::vector<float> get_states_prob();

private:
	int states,replicates,C1,C2;
	int min_obs, range;
	float posterior_threshold,pseudo_count;
	std::vector<float> initial_dist,marginals;
	std::vector<observation_seq>& obs;
	table_2d& transitions;
	table_2d& emissions_p, emissions_r; 
	table_3d _NB_prob_cache;
	/*learning methods*/
	void forward_backward(int col,table_2d&);
	inline float get_joint_prob(int,int,int);
	void decode(int,table_2d_i&,table_2d&);

	/*emissions methods*/
	void estimate_emissions();
	void re_estimate_emissions(table_2d_i&);

	/*transition methods*/
	void estimate_transitions();
	void re_estimate_transitions(table_2d_i&);
	
	void fill_read_counts(table_2d&, std::vector<int>&);
	void estimate_NB_coef(int, table_2d&);



};

#endif /* HMM_H_ */
