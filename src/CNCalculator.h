/*
 * CNCalculator.h
 *
 *  Created on: Apr 30, 2014
 *      Author: haitham
 */

#ifndef CNCALCULATOR_H_
#define CNCALCULATOR_H_


#include <map>
#include <string>
#include<vector>
#include <fstream>
#include <algorithm>
#include <iterator>
#include <sstream>
#include <numeric>
#include <iostream>
#include <cstdlib>
#include <set>

#include "utils.h"
#include "types.h"
class CNCalculator {
public:
	CNCalculator(int);
	virtual ~CNCalculator();
	void read_gc_profile(std::string& );
	void calculate(reads& tags);
	density get_CN();
	void clear();
	void print(std::ofstream&);
	void clean_reads(reads&,reads&);
private:
	int ploidy,large_bin_size;
	density read_count, ratio_profile,GC_profile,medians,notNprofile;
	counts breakpoints;




	void calculateCopyNumberMedians();
	int recalculateRatioUsingCG ();
	double calculateMedianAround (float, float);
	void calculateBreakpoints();
};

#endif /* CNCALCULATOR_H_ */
