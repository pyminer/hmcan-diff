/*************************************************************************
Copyright (c) 2015, Haitham ASHOOR

>>> SOURCE LICENSE >>>
This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation (www.fsf.org); either version 2 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

A copy of the GNU General Public License is available at
http://www.fsf.org/licensing/licenses

>>> END OF LICENSE >>>
*************************************************************************/

#include "Reader.h"
#include "PeakCaller.h"
#include "Parser.h"
#include "utils.h"
#include "types.h"
#include "ConditionsHandler.h"
#include <assert.h>
#include <cmath>
using namespace std;


int main(int argc, char * argv[]){

	print_version();

	if (argc<1){
		cerr<<"Not enough arguments"<<endl<<"Usage: ./HMDiff <Condiftion1FilesList> <Condition1ControlFilesList>"<<
				" <Condiftion2FilesList> <Condition2ControlFilesList> <configuration file> <Name>"<<endl;
		cerr<<"For more details please see README.md"<<endl;
		exit(1);
	}

	//unsigned int i;
	/*parse the input*/
	Parser p = Parser();
	p.parse(argc,argv);
	cout<<"paired end value is "<<p.pairEnd<<endl;
	cout<<"negative binomail "<<p.negbinom<<endl;
	/*Read the data*/
	int min_quality = 10;
	bool remove_duplicates = true;;

    vector<vector<int> > SpikeReadNumbers;
    if (p.useSpikesCounts) {
        SpikeReadNumbers.push_back(read_commaSeparated_list(p.c1_spikeInReadCounts));
        SpikeReadNumbers.push_back(read_commaSeparated_list(p.c2_spikeInReadCounts));
    }

	Reader ReadsReader(p.t,min_quality,remove_duplicates);

	vector<vector<string> > TargetList,ControlList;
	string File;
	reads_matrix chip;
	reads_vector control;


	// Read get target files.
	TargetList.push_back(read_files_list(p.chip_file_c1));
	TargetList.push_back(read_files_list(p.chip_file_c2));
	ControlList.push_back(read_files_list(p.control_file_c1));
	ControlList.push_back(read_files_list(p.control_file_c2));


	for (unsigned int i=0;i<2;i++) {
		if (TargetList[i].empty() || ControlList.empty()){
			cerr<<"Error: You should provide target and control files for both conditions, one or both are missing"<<endl;
			exit(1);
		}
	}

    if (p.useSpikesCounts) {
        if (SpikeReadNumbers.size()!=2) {
            cerr<<"Error: You did not provide spike-in information for all conditions"<<endl;
            exit(1);
        }
    }


	//Reading chip data

	for (unsigned int i=0;i<TargetList.size();i++){

		cout<<"Number of ChIP replicates for condition "<<i+1<<" is:"<<TargetList[i].size()<<endl;
		reads_vector temp_vec;
		for (unsigned int j=0;j<TargetList[i].size();j++){
			reads temp;
			cout<<"Reading ChIP replicate "<<j+1<<"..."<<endl;
			cout<<TargetList[i][j].c_str()<<endl;
			temp = ReadsReader.Read(TargetList[i][j].c_str());
			temp_vec.push_back(temp);
			temp.clear();
		}
		chip.push_back(temp_vec);
		temp_vec.clear();
	}


	//read control data
	for (unsigned int i=0;i<ControlList.size();i++){
		reads temp;
		cout<<"Reading Control data for condition "<<i+1<<"..."<<endl;
		temp = ReadsReader.ReadFromMultipeFiles(ControlList[i]);
		control.push_back(temp);
		temp.clear();
	}

	cout<<"Done reading files!!"<<endl;
	/*read blacklist file*/

	cout<<"Getting blacklist info"<<endl;
	vector<BedEntry> black_list = ReadsReader.Read_blacklist(p.blacklist.c_str());



	float pvalue_theshold = 0.01;
	ConditionsHandler profiler(p.bin_size,p.large_bin_size,p.c1_median_length,p.c1_min_length,p.c1_max_length,
			p.c2_median_length,p.c2_min_length,p.c2_max_length,
			p.merge_dist,pvalue_theshold,p.fold_change,p.path,p.GC_index,black_list,p.negbinom,p.pairEnd,p.useSpikesCounts);
	profiler.profile(chip,control);
	profiler.generate_observations();
	profiler.normalize_conditions(SpikeReadNumbers);
	//profiler.calculate_initial_dists();


	vector<observation_seq> obs = profiler.get_observations();

	//vector<vector<float> >  emissions_p = profiler.get_emissions_p();
	//vector<vector<float> >  emissions_r = profiler.get_emissions_r();
	//table_2d transitions = profiler.get_transitions();





	int min_obs = profiler.get_min_obs();
	int sample_counts = profiler.get_sample_counts();
	int range = profiler.get_range();
	map<string,int> chrom_sizes = profiler.get_chrom_sizes();
	int max_length = profiler.get_max_length_from_conditions();
	int median_length =  profiler.get_med_length_from_conditions();
	int step = (int) ceil(float(max_length)/p.bin_size);
	profiler.clear();

	PeakCaller HMCan = PeakCaller(p.name,obs,step,p.bin_size,sample_counts,
		p.final_threshold,median_length,chip[0].size(),chip[1].size(),p.fold_change,p.c1_label,p.c2_label);
	HMCan.call_peaks(p.merge_dist,p.max_iter, min_obs);

	//write HMCan output files
	HMCan.print();

	if (p.wig)
		HMCan.print_wig(chrom_sizes);
	

	return 0;

}
