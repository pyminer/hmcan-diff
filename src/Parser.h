#ifndef PARSER_H_
#define PARSER_H_
#include <string>
#include "utils.h"
#include "OptionParser.h"
class Parser {
public:
	Parser();
	virtual ~Parser();
	void parse(int argc, char * argv[]);
	void print();
	std::string chip_file_c1,chip_file_c2,control_file_c1, control_file_c2,path,GC_index,blacklist, name, c1_label, c2_label,c1_spikeInReadCounts,c2_spikeInReadCounts;
	int c1_min_length, c1_median_length,c1_max_length,c2_min_length, c2_median_length,c2_max_length,
	bin_size,large_bin_size,merge_dist,max_iter;
	float iter_threshold,final_threshold,pvalue_threshold, posterior_threshold,fold_change;
	bool wig,posterior,negbinom,pairEnd,useSpikesCounts;
	Format t;


};

#endif /* PARSER_H_ */
