/*************************************************************************
Copyright (c) 2015, Haitham ASHOOR

>>> SOURCE LICENSE >>>
This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation (www.fsf.org); either version 2 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

A copy of the GNU General Public License is available at
http://www.fsf.org/licensing/licenses

>>> END OF LICENSE >>>
*************************************************************************/

#include "Parser.h"
#include <fstream>
#include <iostream>
#include<algorithm>
using namespace std;
using namespace optparse;

#include <cstdlib>

Parser::Parser() {

}

Parser::~Parser() {
	// TODO Auto-generated destructor stub
}

void Parser::parse(int argc, char * argv[]){

	 const string usage = "usage: HMCan-diff [OPTION]";

	  const string version = "HMCan-diff V 1.0: \nCopyright (c) 2015, Haitham ASHOOR \n"
			  "This program is free software; you can redistribute it and/or modify\n"
			  "it under the terms of the GNU General Public License as published by\n"
			  "the Free Software Foundation (www.fsf.org); either version 2 of the\n"
			  "License, or (at your option) any later version.\n"
			  "This program is distributed in the hope that it will be useful,\n"
			  "but WITHOUT ANY WARRANTY; without even the implied warranty of\n"
			  "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n"
			  "GNU General Public License for more details.\n"
			  "A copy of the GNU General Public License is available at\n"
			  "http://www.fsf.org/licensing/licenses\n";


	  const string desc = "HMCan-diff is a program to identify differential histone modifications regions from cancer ChIP-seq samples";


	  OptionParser parser = OptionParser()
	    .usage(usage)
	    .version(version)
	    .description(desc);

	  parser.add_option("--name").help("Prefix name for all output files").set_default(""); //maybe I should remove this one
	  parser.add_option("--C1_label").help("Name for condition 1. Default: C1").set_default("C1");
	  parser.add_option("--C2_label").help("Name for condition 2. Default: C2").set_default("C2");
	  parser.add_option("--C1_ChIP").help("Path for file contains condition 1 ChIP file names").set_default("");
	  parser.add_option("--C2_ChIP").help("Path for file contains condition 2 ChIP file names").set_default("");
	  parser.add_option("--C1_Control").help("Path for file contains condition 1 Input file names").set_default("");
	  parser.add_option("--C2_Control").help("Path for file contains condition 2 Input file names").set_default("");
	  parser.add_option("--format").help("HMCan-diff accepts BED,SAM, and BAM formats").set_default("");
	  parser.add_option("--genomePath").help("Path for the target genome FASTA files.").set_default("");
	  parser.add_option("--GCProfile").help("Path for the GC content information file").set_default("");
	  parser.add_option("--C1_minLength").type("int").action("store").help("Condition 1 minimum fragment length").set_default(-1);
	  parser.add_option("--C1_medLength").type("int").action("store").help("Condition 1 median fragment length").set_default(-1);
	  parser.add_option("--C1_maxLength").type("int").action("store").help("Condition 1 maximum fragment length").set_default(-1);
	  parser.add_option("--C2_minLength").type("int").action("store").help("Condition 2 minimum fragment length").set_default(-1);
	  parser.add_option("--C2_medLength").type("int").action("store").help("Condition 2 median fragment length").set_default(-1);
	  parser.add_option("--C2_maxLength").type("int").action("store").help("Condition 2 maximum fragment length").set_default(-1);
	  parser.add_option("--smallBin").type("int").action("store").set_default(50).help("Size of the bin to calculate density. "
			  "default: %default");
	  parser.add_option("--largeBin").type("int").action("store").set_default(100000).help("Size of the bin to calculate copy number. "
	  		  "default: %default");
	  parser.add_option("--negativeBinomial").type("bool").action("store_true").dest("negbinom").help("Use negative binomail distribution to "
			  "identify enriched regions. Default: use local Poisson distribution");
	  //parser.add_option("--pvalueThreshold").type("float").action("store").set_default(0.01).help("Threshold to consider a bin enriched or not. "
	  //		  "default: %default");
	  parser.add_option("--mergeDistance").type("int").action("store").set_default(1000).help("Distance to merge two peaks into one. "
			  "Default: %default");
	  //parser.add_option("--iterationThreshold").type("float").action("store").set_default(0).help("Threshold for HMCan learning process. "
		//		  "Default: %default");
	  parser.add_option("--pvalue").type("float").action("store").set_default(0.05).help("Threshold for differential regions P-value "
			  "Default: %default");

	  parser.add_option("--maxIter").type("int").action("store").set_default(20).help("Max number of iterations for HMCan. "
			  "Default:%default");

	  //parser.add_option("--PosteriorProb").type("float").action("store").set_default(0.7).help("Minimum posterior probability"
	  //		  "for posterior decoding process. Default:%default");

	  parser.add_option("--PrintWig").type("bool").action("store_true").dest("signal_wig").help("Print wig files for normalized signal");
	  //parser.add_option("--printPosterior").type("bool").action("store_true").dest("prob_wig").help("print wig files for posterior probability");
	  parser.add_option("--blackListFile").help("Path for bed file contains regions to ignore");
	  parser.add_option("--fold_change").action("store").type("float").set_default(2).help("Fold change threshold to consider a bin "
			  "differential. Default: %default");
	  parser.add_option("--pairedEnd").type("bool").action("store_true").dest("pairEnd").help("Enable paired end processing in HMCan-diff");

	  // add spikes-in group
      parser.add_option("--C1_spikeInReadCounts").help("Comma-delimited read counts from spike-in experiments for Condition C1: e.g.: 123043,232424,343435").set_default("");
	  parser.add_option("--C2_spikeInReadCounts").help("Comma-delimited read counts from spike-in experiments for Condition C2").set_default("");


	  Values& options = parser.parse_args(argc, argv);


	  //get arguments


	  chip_file_c1 =  (string) options.get("C1_ChIP");
	  chip_file_c2 = (string) options.get("C2_ChIP");

	  if (chip_file_c1.size()==0 || chip_file_c2.size()==0){
	  	  cerr<<"File contining ChIP files is not provided. Please provide such file. Exiting..."<<endl;
	  	  exit(1);
      }


	  control_file_c1 = (string) options.get("C1_Control");
	  control_file_c2 = (string) options.get("C2_Control");

	  if (control_file_c1.size()==0 || control_file_c2.size()==0){
	   	  cerr<<"File contining Input files is not provided. Please provide such file. Exiting..."<<endl;
	   	  exit(1);
	  }

      c1_spikeInReadCounts = (string) options.get("C1_spikeInReadCounts");
	  c2_spikeInReadCounts = (string) options.get("C2_spikeInReadCounts");
	  if (c1_spikeInReadCounts.size()>0 && c2_spikeInReadCounts.size()>0) {
        cout << "\t..Will use spike-in information:\nC1 read counts "<<c1_spikeInReadCounts<<"\nC2 read counts "<<c2_spikeInReadCounts<<endl;
        cout << "\t..Will not adjust densities using library size or signal to noise ratio"<<endl;
        useSpikesCounts=true;
	  } else {
        cout << "\t..Will not use spike-in information"<<endl;
        useSpikesCounts = false;
	  }

	  blacklist = (string) options.get("blackListFile");
	  if (blacklist.size()==0){
		  cerr<<"Error: please provide path for blacklist file"<<endl;
		  exit(1);
	  }

	  //FORMAT has to be parsed into the switch statment

	  string temp_type = (string) options.get("format");
	  if (temp_type.size()==0){
		  cerr <<"Please provide alignment files format. HMCan-diff support BAM, SAM, and BED. Exiting..."<<endl;
		  exit(1);
	  }


	  transform(temp_type.begin(), temp_type.end(),temp_type.begin(), ::tolower);
	  if (temp_type.compare("bed")==0)
		  t = BED;
	  else if (temp_type.compare("sam")==0)
		  t = SAM;
	  else if (temp_type.compare("bam")==0)
		  t = BAM;
	  else{
		  cerr<<"Please provide a file in SAM, BAM or BED formats."<<endl;
		  exit(EXIT_FAILURE);
	  	}



	  path = (string) options.get("genomePath");
	  if (path.size()==0){
		  cerr<<"Please provide the path for the folder containing fasta file of the target genome. Exiting..."<<endl;
		  exit(1);
	  }


	  GC_index = (string) options.get("GCProfile");
	  if (GC_index.size()==0){
		  cerr<<"Please provide path for the file containing GC content information.Exiting..."<<endl;
		  exit(1);
	  }



	  c1_min_length = (int) options.get("C1_minLength");
	  c1_median_length = (int) options.get("C1_medLength");
	  c1_max_length = (int) options.get("C1_maxLength");

	  c2_min_length = (int) options.get("C2_minLength");
	  c2_median_length = (int) options.get("C2_medLength");
	  c2_max_length = (int) options.get("C2_maxLength");
	  pairEnd = (bool) options.get("pairEnd");


	  if (pairEnd){
		  cerr<<"Consider your datasets are Paired-End..."<<endl;
	  }else {
          cerr<<"Consider your datasets are Single-End..."<<endl;
	  }

	  if ((c1_min_length<0 || c1_median_length<0 || c1_max_length<0) && !pairEnd){
		  cerr<<"Please provide correct fragment lengths for condition 1. Exiting..."<<endl;
		  exit(1);
	  }

	  if ((c2_min_length<0 || c2_median_length<0 || c2_max_length<0) && !pairEnd){
	 	  cerr<<"Please provide correct fragment lengths for condition 2. Exiting..."<<endl;
	 	  exit(1);
	  }


	  bin_size = (int) options.get("smallBin");
	  if (bin_size<=0){
		  cerr<<"Info: HMCan-diff will use smallBinLength default value 50."<<endl;
		  bin_size = 50;
	  }


	  large_bin_size = (int) options.get("largeBin");
	  if (large_bin_size <= 0){
		  cerr<<"Info: HMCan-diff will use largeBinLength default value 10000."<<endl;
		  large_bin_size = 100000;
	  }


	  merge_dist = (int) options.get("mergeDistance");
	  if (merge_dist <= 0){
	 	  cerr<<"Info: HMCan-diff will use mergeDistance default value 1000."<<endl;
	 	  merge_dist = 1000;
	   }


	  max_iter = (int) options.get("maxIter");
	  if (max_iter<=0){
		  cerr<<"Info: HMCan-diff will use maxIter default value 20."<<endl;
		  max_iter=20;
	  }

	  //iter_threshold = (float) options.get("iterationThreshold");
	  //if (iter_threshold < 0){
	  //	  cerr<<"Info: HMCan-diff will use iterationThreshold default value 0."<<endl;
	  //	  iter_threshold=0;
	  // }


	  final_threshold = (float) options.get("pvalue");
	  if (final_threshold <= 0){
	  	  cerr<<"Info: HMCan-diff will use finalThreshold default value 0.1."<<endl;
	  	  final_threshold=0.1;
	  }


	  /*
	  pvalue_threshold = (float) options.get("pvalueThreshold");
	  if (pvalue_threshold <= 0 || pvalue_threshold>1 ){
	    	  cerr<<"Info: HMCan-diff will use pvalueThreshold  default value 0.01."<<endl;
	    	  pvalue_threshold=0.01;
	  }

	  posterior_threshold = (float) options.get("PosteriorProb");
	  if (posterior_threshold <=0 || posterior_threshold>1){
		  cerr<<"Info: HMCan-diff will use PosteriorProb default value 0.7"<<endl;
		  posterior_threshold = 0.7;
	  }*/


	  fold_change = (float) options.get("fold_change");
	  if (fold_change <=0){
		  cerr<<"Info: HMCan-diff will use fold_change default value 2"<<endl;
		  fold_change = 2;
	  }


	  wig = (bool) options.get("signal_wig");
	  //posterior = (bool) options.get("prob_wig");
	  negbinom = (bool) options.get("negbinom");

	  //pairEnd = true;


	  name = (string) options.get("name");
	  if (name.size()==0){
		  cerr<<"Info: experiment name is set to test"<<endl;
		  name = "test";
	  }

	  c1_label = (string) options.get("C1_label");
	  c2_label = (string) options.get("C2_label");

}


void Parser::print(){
	cout<<GC_index<<endl;
	cout<<path<<endl;
	//cout<<min_length<<endl;
	//cout<<median_length<<endl;
	//cout<<max_length<<endl;
	cout<<large_bin_size<<endl;
	cout<<bin_size<<endl;
	cout<<pvalue_threshold<<endl;
	cout<<merge_dist<<endl;
	cout<<iter_threshold <<endl;
	cout<<final_threshold <<endl;
	cout<<max_iter<<endl;
	cout<<blacklist<<endl;
}
