/*
 * ConditionsHandler.h
 *
 *  Created on: Dec 1, 2014
 *      Author: haitham
 */

#ifndef CONDITIONSHANDLER_H_
#define CONDITIONSHANDLER_H_

#include <vector>
#include "types.h"

class ConditionsHandler {
public:
	ConditionsHandler(int,int,int,int,int,int,int,int,int,float,float, std::string&, std::string&,std::vector<BedEntry>&, bool, bool,bool);
	virtual ~ConditionsHandler();
	void profile(reads_matrix& chip, reads_vector& control);

	void generate_observations();
	void normalize_conditions(std::vector<std::vector<int> > &SpikeReadNumbers);
	void calculate_initial_dists();
	void clear();

	int get_min_obs();
	int get_sample_counts();
	std::vector<observation_seq> get_observations();
	std::vector<std::vector<float> > get_emissions_p();
	std::vector<std::vector<float> > get_emissions_r();
	table_2d get_transitions();
	int get_range();
	std::map<std::string,int> get_chrom_sizes();
	int get_max_length_from_conditions();
	int get_med_length_from_conditions();

private:

	int conditions_count, bin_size, large_bin_size, min_dist,max_obs,min_obs, step,total_counts,rows,min_obs_size,range;

	int min_f_length[2], median_f_length[2], max_f_length[2];

	float threshold,fold_change;
	bool pairedEnd,negbinom,useSpikesCounts;
	std::vector<int> control_counts,cols;
	table_2d_i chip_counts;
	table_2d noise_ratio;
	table_2d expected_signal;

	std::vector<std::string> chr_list, control_max_chr;
	std::vector<std::vector<std::string> > chip_max_chr;

	std::map<std::string,int> sampled_sizes, sizes;
	std::vector<observation_seq> obs;
	std::vector<BedEntry> black_list;


	density GC_profile;
	density_vector CNVs;
	density_vector temp_control;
	density_matrix target,control; //this matrix contains all conditions and all replicates
	counts_matrix peaks_final;
	counts peaks_final_sum;
	std::string path, gc_index;
	table_2d transitions;
	//std::vector<std::map<std::vector<int>,float > > emissions;
	std::vector<std::vector <float> > emissions_r, emissions_p; //vector of NB coeficients.

	void normalize_all_independent();
	void normalize_single_condition(density_vector&,density&,density&,std::vector<int>&,int,int);
	void clean_reads(reads_matrix&, reads_vector&);
	void build_denities(reads_matrix&,reads_vector&);
	void build_gc_profile();
	void build_CNV_profile(reads_vector&);
	void count_reads(reads_matrix&,reads_vector&);
	counts get_peaks(density& ,density& );
	void remove_singeltons(counts&);
	void merge_peaks(counts&);


	void estimate_reads_counts_at_region(std::vector<int>&, std::vector<int>&);
	void inter_conditions_normalization_library_size();
	void inter_conditions_normalization_regression();
	void inter_condition_normalization_sequence_depth();
	void inter_condition_normalization_spikein(std::vector<std::vector<int> > &SpikeReadNumbers);
	bool agree_on_state(std::string&, int, int);
	void get_initial_dists();
	void estimate_frag_lengths(reads_matrix&);
	void fill_read_counts(std::vector<std::vector<float> >&, std::vector<float>&, std::vector<float>&);
	void estimate_NB_coef(int, std::vector<std::vector<float> >&);



};

#endif /* CONDITIONSHANDLER_H_ */
