/*
 * FragLengthCalculator.h
 *
 *  Created on: May 21, 2017
 *      Author: ashoorhm
 */

#ifndef SRC_FRAGLENGTHCALCULATOR_H_
#define SRC_FRAGLENGTHCALCULATOR_H_


#include<vector>
#include "types.h"

class FragLengthCalculator {
public:
	FragLengthCalculator();
	virtual ~FragLengthCalculator();
	std::vector<int> getFragmentLengthDist(std::vector<DNA_fragment>&);

private:
	int left,right,med;
	void ComputeFragLengthDist(std::vector<int>&);
};

#endif /* SRC_FRAGLENGTHCALCULATOR_H_ */
