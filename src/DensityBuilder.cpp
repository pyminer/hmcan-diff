/*************************************************************************
Copyright (c) 2015, Haitham ASHOOR

>>> SOURCE LICENSE >>>
This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation (www.fsf.org); either version 2 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

A copy of the GNU General Public License is available at
http://www.fsf.org/licensing/licenses

>>> END OF LICENSE >>>
*************************************************************************/

#include "DensityBuilder.h"
using namespace std;


DensityBuilder::DensityBuilder(map<string,int>& _sizes,
		map<string,int>& _sampled_sizes ,int bin_length,int med,int left,int right, bool pairedEnd):
		sampled_sizes(_sampled_sizes), sizes(_sizes){
	this->bin_length = bin_length;
	this->med = med;
	this->left = left;
	this->right = right;
	this->pairedEnd = pairedEnd;

	// if single end we can calculate the density coefs at the constructor,
	// otherwise after we calculate the coefs vlaues
	//cout<<"Fragment length dist is: "<<this->left<<" "<<this->med<<" "<<this->right<<endl;


	calculateDensityCoef();

}

DensityBuilder::~DensityBuilder() {
	// TODO Auto-generated destructor stub
}


void DensityBuilder:: extend_reads(std::vector<DNA_fragment >& tags, int chr_size){

	for(vector<DNA_fragment>::iterator tag_it = tags.begin(); tag_it!=tags.end();++tag_it)
		if ((*tag_it).strand == 1) {
			if ((*tag_it).fragmentLength>0) {
				if ((*tag_it).start-(*tag_it).fragmentLength-1<0)
					(*tag_it).start = 0;
				else
					(*tag_it).start = (*tag_it).start-(*tag_it).fragmentLength+1;
			}
			else {
				if ((*tag_it).start-right-1<0)
					(*tag_it).start = 0;
				else
					(*tag_it).start = (*tag_it).start-right+1;
	            }
		}



	//remove out of range items
	vector<DNA_fragment>::iterator it = tags.begin();
	while(it!=tags.end()){
		if ((*it).start >= chr_size)
			it = tags.erase(it);

		else
			++it;

	}

	sort(tags.begin(),tags.end(),compare); // resort to take extension into consideration

}

void DensityBuilder::build_single_profile(vector<DNA_fragment >& tags, string chr){

	// allocate memory for density
	profile_density[chr] = vector<float>(sampled_sizes[chr],0);
	for(vector<DNA_fragment>::iterator fragment_it=tags.begin(); fragment_it !=tags.end();++fragment_it ){
		if ((*fragment_it).fragmentLength==0 || (*fragment_it).fragmentLength>right) {
			int fragment_start,fragment_left,fragment_med, fragment_right;
			if ((*fragment_it).strand==0) { //forward read:

				fragment_start = (*fragment_it).start;
				fragment_left  = (*fragment_it).start+left-1>sizes[chr] ? sizes[chr]-1:(*fragment_it).start+left-1;
				fragment_med = 	(*fragment_it).start+med-1>sizes[chr]?sizes[chr]-1:(*fragment_it).start+med-1;
				fragment_right = (*fragment_it).start+right-1>sizes[chr]? sizes[chr]-1:(*fragment_it).start+right-1;

				//start at multiple of bins position
				int start_point = fragment_start;
				while ((start_point+1)%bin_length!=0)
					start_point++;

				//calculate density using triangular distribution
				//check findpeaks program for more info
				for(int i=start_point;i<=fragment_right;i+=bin_length){
					int index = ((i)/bin_length);
					if(index>=sampled_sizes[chr]){
						cerr<<"Incorrect reference genome, please check your reference genome"<<endl;
						exit(1);
					}
					int x = i-fragment_start;
					if (i<=fragment_left)
						profile_density[chr][index]+=1;
					else if (i<=fragment_med){
						float hx = slope1 * x + b1;
						profile_density[chr][index]+= 1 - ((hx * (x-left)) /2);
					}
					else{
						float hx = slope2 * x + b2;
						float point_density = (1 - left_area)-(right_area - (hx * (float)(right-x) /2));
						profile_density[chr][index]+=point_density;
					}


				}
			}
			else { //reverse read
				fragment_start = (*fragment_it).start+right-1>sizes[chr]? sizes[chr]-1:(*fragment_it).start+right-1; //rightmost point
				fragment_left  = (*fragment_it).start; //leftmost point
				fragment_med =  fragment_start - med;
				fragment_right = fragment_start - left;
				if (fragment_med<0)
					fragment_med=0;
				if (fragment_right<0)
					fragment_right=0;

				 //start at multiple of bins position
				int start_point = fragment_left;
				while ((start_point+1)%bin_length!=0)
					start_point++;

				 for(int i=start_point;i<=fragment_start;i+=bin_length){
					int index = ((i+1)/bin_length)-1;
					if(index>=sampled_sizes[chr]){
						cerr<<"Incorrect reference genome, please check your reference genome"<<endl;
						exit(0);
					}

					int x = fragment_start-i;
					if (i>=fragment_right)
						profile_density[chr][index]+=1;
					else if (i>=fragment_med){
						float hx = slope1 * x + b1;
						profile_density[chr][index]+= 1 - ((hx * (x-left)) /2);

					}
					else{
						float hx = slope2 * x + b2;
						float point_density = (1 - left_area)-(right_area - (hx * (float)(right-x) /2));
						profile_density[chr][index]+=point_density;

					}
				}
			}

		}

		else{ // paired end reads

			int fragment_start, fragment_right;
			fragment_start = (*fragment_it).start;
			fragment_right = (*fragment_it).start+(*fragment_it).fragmentLength-1>sizes[chr]? sizes[chr]-1:(*fragment_it).start+(*fragment_it).fragmentLength-1;

			//start at multiple of bins position
			int start_point = fragment_start;
			while ((start_point+1)%bin_length!=0)
				start_point++;


			//add 1 to density
			for(int i=start_point;i<=fragment_right;i+=bin_length){
				int index = ((i+1)/bin_length)-1;
				if(index>=sampled_sizes[chr]){
					cerr<<"Incorrect reference genome, please check your reference genome"<<endl;
					exit(0);
				}
				profile_density[chr][index]+=1;

			}






		}

	}


	if (pairedEnd){// since we count the fragment twice in the paired end, we adjust this
		for (int i=0;i<profile_density[chr].size();i++)
			profile_density[chr][i]/=2.0;
	}


}


void DensityBuilder::build_density(reads& tags){


	for (map<string,vector<DNA_fragment> >::iterator chr_it = tags.begin();chr_it!=tags.end();++chr_it){
		/*if (right==0){
			calculateFragLengthDist((*chr_it).second);
			calculateDensityCoef();
		}*/

		if ((*chr_it).first.compare("chrM")==0)
			continue;

		extend_reads((*chr_it).second,sizes[(*chr_it).first]);
		build_single_profile((*chr_it).second,(*chr_it).first);
	}

}



void DensityBuilder::calculateDensityCoef(){
	h = 2/(float)(right-left);
	slope1 = h/(float)(med-left);
	slope2 = -h/(float)(right-med);
	b1 = -slope1*left;
	b2 = -slope2*right;
	left_area = (h*(float)(med-left))/2;
	right_area = (h*(float)(right-med)/2);
}

density DensityBuilder::get(){
	return profile_density;
}

void DensityBuilder::clear(){
	map<string,vector<float> >::iterator chr_it;
	for(chr_it=profile_density.begin();chr_it!=profile_density.end();++chr_it)
		(*chr_it).second.clear();

	profile_density.clear();
}
